#include <string.h>

#include "json.h"

int json_get_int_key(char* buf, char* json_key, int* result) {
    json_object * jobj = json_tokener_parse(buf);
    enum json_type buf_type = json_object_get_type(jobj);
    enum json_type key_type;
    if (buf_type == json_type_object) {
        json_object_foreach(jobj, key, val) {
            key_type = json_object_get_type(val);            
            if (key_type == json_type_int && strcmp(key, json_key) == 0) {
                *result = json_object_get_int(val);
                return 0;
            }
        }
    }
    return -1;
}