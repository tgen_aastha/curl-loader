/*
*     url.c
*
* 2007 Copyright (c) 
* Robert Iakobashvili, <coroberti@gmail.com>
* All rights reserved.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "url.h"

double get_random ();

int
current_url_completion_timeout (unsigned long *timeout, 
                                url_context* url, 
                                unsigned long now)
{
  (void) now;

  if (!timeout || ! url)
    {
      fprintf(stderr, "%s error: wrong input.\n", __func__);
      return -1;
    }

  if (! url->timer_url_completion_hrange)
    {
      *timeout = url->timer_url_completion_lrange;
      return 0;
    }

  *timeout = url->timer_url_completion_lrange + 
          (unsigned long) (url->timer_url_completion_hrange * get_random());

  return 0;
}

int
current_url_sleeping_timeout (unsigned long *timeout, 
                              url_context* url, 
                              long delay,
                              unsigned long now)
{
  (void) now;

  if (!timeout || ! url)
    {
      fprintf(stderr, "%s error: wrong input.\n", __func__);
      return -1;
    }
  
  if (! url->timer_after_url_sleep_hrange)
    {
      *timeout = url->timer_after_url_sleep_lrange + delay;
      return 0;
    }

  *timeout = url->timer_after_url_sleep_lrange + 
          (unsigned long) (url->timer_after_url_sleep_hrange * get_random()) + delay;

  return 0;
}

/******************************************************************************
* Function name - url_schema_classification
*
* Description - Makes url analyses to return the type (e.g. http, ftps, telnet, etc)
*
* Input -      *url - pointer to the url context
* Return Code/Output - On success - a url schema type, on failure -
*                    (URL_APPL_UNDEF)
*******************************************************************************/
url_appl_type url_schema_classification (const char* const url)
{
  if (!url)
    {
      return  URL_APPL_UNDEF;
    }

  if (strstr (url, HTTPS_SCHEMA_STR))
    return URL_APPL_HTTPS;
  else if (strstr (url, HTTP_SCHEMA_STR))
    return URL_APPL_HTTP;
  else if (strstr (url, FTPS_SCHEMA_STR))
    return URL_APPL_FTPS;
  else if (strstr (url, FTP_SCHEMA_STR))
    return URL_APPL_FTP;
  else if (strstr (url, SFTP_SCHEMA_STR))
    return URL_APPL_SFTP;
  else if (strstr (url, TELNET_SCHEMA_STR))
    return URL_APPL_TELNET;
  else if (strstr (url, SMTP_SCHEMA_STR))
    return URL_APPL_SMTP;
  else if (strstr (url, SMTPS_SCHEMA_STR))
    return URL_APPL_SMTPS;
  else if (strstr (url, POP3_SCHEMA_STR))
    return URL_APPL_POP3;
  else if (strstr (url, POP3S_SCHEMA_STR))
    return URL_APPL_POP3S;
  else if (strstr (url, IMAP_SCHEMA_STR))
    return URL_APPL_IMAP;
  else if (strstr (url, IMAPS_SCHEMA_STR))
    return URL_APPL_IMAPS;

  return  URL_APPL_UNDEF;
}
