#include <json/json.h>

#define json_object_foreach(obj,key,val)\
char *key; struct json_object *val;\
for(\
  struct lh_entry *entry = json_object_get_object(obj)->head;\
  ({ if(entry) { key = (char*)entry->k; val = (struct json_object*)entry->v; } ; entry; });\
  entry = entry->next\
)

int json_get_int_key(char* buf, char* json_key, int* result);