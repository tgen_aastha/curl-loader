/*
*     client.c
*
* 2006-2007 Copyright (c) 
* Robert Iakobashvili, <coroberti@gmail.com>
* All rights reserved.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// must be the first include
#include "fdsetsize.h"

#include <stdlib.h>
#include <errno.h>

#include "client.h"
#include "batch.h"


/*
  Accessors to the flags-counters of headers.
  Incrementing counters for the flags-counters of headers.
*/
int first_hdr_req (client_context* cctx)
{
  return cctx->first_hdr_req;
}
void first_hdr_req_inc (client_context* cctx)
{
  cctx->first_hdr_req++;
}
int first_hdr_1xx (client_context* cctx)
{
  return cctx->first_hdr_1xx;
}
void first_hdr_1xx_inc (client_context* cctx)
{
  cctx->first_hdr_1xx++;
}
int first_hdr_2xx (client_context* cctx)
{
  return cctx->first_hdr_2xx;
}
void first_hdr_2xx_inc (client_context* cctx)
{
  cctx->first_hdr_2xx++;
}
int first_hdr_3xx (client_context* cctx)
{
  return cctx->first_hdr_3xx++;
}
void first_hdr_3xx_inc (client_context* cctx)
{
  cctx->first_hdr_3xx++;
}

int first_hdr_4xx (client_context* cctx)
{
  return cctx->first_hdr_4xx;
}
void first_hdr_4xx_inc (client_context* cctx)
{
  cctx->first_hdr_4xx++;
}
int first_hdr_5xx (client_context* cctx)
{
  return cctx->first_hdr_5xx;
}
void first_hdr_5xx_inc (client_context* cctx)
{
  cctx->first_hdr_5xx++;
}
int first_hdr_6xx (client_context* cctx)
{
  return cctx->first_hdr_6xx;
}
void first_hdr_6xx_inc (client_context* cctx)
{
  cctx->first_hdr_6xx++;
}


/*
  Reseting to zero counters for the flags-counters of headers.
*/
void first_hdrs_clear_all (client_context* cctx)
{
  cctx->first_hdr_req = cctx->first_hdr_1xx = cctx->first_hdr_2xx = 
    cctx->first_hdr_3xx = cctx->first_hdr_4xx = cctx->first_hdr_5xx =
      cctx->first_hdr_6xx = 0;
}
void first_hdrs_clear_non_req (client_context* cctx)
{
  cctx->first_hdr_1xx = cctx->first_hdr_2xx = cctx->first_hdr_3xx = 
    cctx->first_hdr_4xx = cctx->first_hdr_5xx = cctx->first_hdr_6xx = 0;
}
void first_hdrs_clear_non_1xx (client_context* cctx)
{
  cctx->first_hdr_req = cctx->first_hdr_2xx = cctx->first_hdr_3xx =
    cctx->first_hdr_4xx = cctx->first_hdr_5xx = cctx->first_hdr_6xx = 0;
}
void first_hdrs_clear_non_2xx (client_context* cctx)
{
  cctx->first_hdr_req = cctx->first_hdr_1xx = cctx->first_hdr_3xx = cctx->first_hdr_4xx =
    cctx->first_hdr_5xx = cctx->first_hdr_6xx = 0;
}
void first_hdrs_clear_non_3xx (client_context* cctx)
{
  cctx->first_hdr_req = cctx->first_hdr_1xx = cctx->first_hdr_2xx = cctx->first_hdr_4xx =
    cctx->first_hdr_5xx = cctx->first_hdr_6xx = 0;
}
void first_hdrs_clear_non_4xx (client_context* cctx)
{
  cctx->first_hdr_req = cctx->first_hdr_1xx = cctx->first_hdr_2xx = cctx->first_hdr_3xx =
    cctx->first_hdr_5xx = cctx->first_hdr_6xx = 0;
}
void first_hdrs_clear_non_5xx (client_context* cctx)
{
  cctx->first_hdr_req = cctx->first_hdr_1xx = cctx->first_hdr_2xx = cctx->first_hdr_3xx =
    cctx->first_hdr_4xx = cctx->first_hdr_6xx = 0;
}

void first_hdrs_clear_non_6xx (client_context* cctx)
{
  cctx->first_hdr_req = cctx->first_hdr_1xx = cctx->first_hdr_2xx = cctx->first_hdr_3xx =
    cctx->first_hdr_4xx = cctx->first_hdr_5xx = 0;
}


void stat_data_out_add (client_context* cctx, url_appl_type protocol, unsigned long bytes)
{
  switch (protocol) {
    case URL_APPL_HTTP:
      cctx->bctx->http_delta.data_out += bytes;
      break;
    case URL_APPL_HTTPS:
      cctx->bctx->https_delta.data_out += bytes;
      break;
    case URL_APPL_FTP:
      cctx->bctx->ftp_delta.data_out += bytes;
      break;
    case URL_APPL_FTPS:
      cctx->bctx->ftps_delta.data_out += bytes;
      break;
    case URL_APPL_IMAP:
      cctx->bctx->imap_delta.data_out += bytes;
      break;
    case URL_APPL_POP3:
      cctx->bctx->pop3_delta.data_out += bytes;
      break;
    case URL_APPL_SMTP:
      cctx->bctx->smtp_delta.data_out += bytes;
      break;
    default:
      break;
  }
}

void stat_data_in_add (client_context* cctx, url_appl_type protocol, unsigned long bytes)
{
  switch (protocol) {
    case URL_APPL_HTTP:
      cctx->bctx->http_delta.data_in += bytes;
      break;
    case URL_APPL_HTTPS:
      cctx->bctx->https_delta.data_in += bytes;
      break;
    case URL_APPL_FTP:
      cctx->bctx->ftp_delta.data_in += bytes;
      break;
    case URL_APPL_FTPS:
      cctx->bctx->ftps_delta.data_in += bytes;
      break;
    case URL_APPL_IMAP:
      cctx->bctx->imap_delta.data_in += bytes;
      break;
    case URL_APPL_POP3:
      cctx->bctx->pop3_delta.data_in += bytes;
      break;
    case URL_APPL_SMTP:
      cctx->bctx->smtp_delta.data_in += bytes;
      break;
    default:
      break;
  }
}

void stat_err_inc (client_context* cctx, url_appl_type protocol)
{
  switch (protocol) {
    case URL_APPL_HTTP:
      cctx->bctx->http_delta.other_errs++;
      break;
    case URL_APPL_HTTPS:
      cctx->bctx->https_delta.other_errs++;
      break;
    case URL_APPL_FTP:
      cctx->bctx->ftp_delta.other_errs++;
      break;
    case URL_APPL_FTPS:
      cctx->bctx->ftps_delta.other_errs++;
      break;
    case URL_APPL_IMAP:
      cctx->bctx->imap_delta.other_errs++;
      break;
    case URL_APPL_POP3:
      cctx->bctx->pop3_delta.other_errs++;
      break;
    case URL_APPL_SMTP:
      cctx->bctx->smtp_delta.other_errs++;
      break;
    default:
      break;
  }
}

void stat_url_timeout_err_inc (client_context* cctx, url_appl_type protocol)
{
  switch (protocol) {
    case URL_APPL_HTTP:
      cctx->bctx->http_delta.url_timeout_errs++;
      break;
    case URL_APPL_HTTPS:
      cctx->bctx->https_delta.url_timeout_errs++;
      break;
    case URL_APPL_FTP:
      cctx->bctx->ftp_delta.url_timeout_errs++;
      break;
    case URL_APPL_FTPS:
      cctx->bctx->ftps_delta.url_timeout_errs++;
      break;
    case URL_APPL_IMAP:
      cctx->bctx->imap_delta.url_timeout_errs++;
      break;
    case URL_APPL_POP3:
      cctx->bctx->pop3_delta.url_timeout_errs++;
      break;
    case URL_APPL_SMTP:
      cctx->bctx->smtp_delta.url_timeout_errs++;
      break;
    default:
      break;
  }
}
void stat_conn_inc (client_context* cctx, url_appl_type protocol, long num_conn)
{
  switch (protocol) {
    case URL_APPL_HTTP:
      cctx->bctx->http_delta.connections += num_conn;
      break;
    case URL_APPL_HTTPS:
      cctx->bctx->https_delta.connections += num_conn;
      break;
    case URL_APPL_FTP:
      cctx->bctx->ftp_delta.connections += num_conn;
      break;
    case URL_APPL_FTPS:
      cctx->bctx->ftps_delta.connections += num_conn;
      break;
    case URL_APPL_IMAP:
      cctx->bctx->imap_delta.connections += num_conn;
      break;
    case URL_APPL_POP3:
      cctx->bctx->pop3_delta.connections += num_conn;
      break;
    case URL_APPL_SMTP:
      cctx->bctx->smtp_delta.connections += num_conn;
      break;
    default:
      break;
  }
}
void stat_req_inc (client_context* cctx, url_appl_type protocol)
{
  switch (protocol) {
    case URL_APPL_HTTP:
      cctx->bctx->http_delta.requests++;
      break;
    case URL_APPL_HTTPS:
      cctx->bctx->https_delta.requests++;
      break;
    case URL_APPL_FTP:
      cctx->bctx->ftp_delta.requests++;
      break;
    case URL_APPL_FTPS:
      cctx->bctx->ftps_delta.requests++;
      break;
    case URL_APPL_IMAP:
      cctx->bctx->imap_delta.requests++;
      break;
    case URL_APPL_POP3:
      cctx->bctx->pop3_delta.requests++;
      break;
    case URL_APPL_SMTP:
      cctx->bctx->smtp_delta.requests++;
      break;
    default:
      break;
  }
}
void stat_1xx_inc (client_context* cctx, url_appl_type protocol)
{
  switch (protocol) {
    case URL_APPL_HTTP:
      cctx->bctx->http_delta.resp_1xx++;
      break;
    case URL_APPL_HTTPS:
      cctx->bctx->https_delta.resp_1xx++;
      break;
    case URL_APPL_FTP:
      cctx->bctx->ftp_delta.resp_1xx++;
      break;
    case URL_APPL_FTPS:
      cctx->bctx->ftps_delta.resp_1xx++;
      break;
    default:
      break;
  }
}
void stat_2xx_inc (client_context* cctx, url_appl_type protocol)
{
  switch (protocol) {
    case URL_APPL_HTTP:
      cctx->bctx->http_delta.resp_2xx++;
      break;
    case URL_APPL_HTTPS:
      cctx->bctx->https_delta.resp_2xx++;
      break;
    case URL_APPL_FTP:
      cctx->bctx->ftp_delta.resp_2xx++;
      break;
    case URL_APPL_FTPS:
      cctx->bctx->ftps_delta.resp_2xx++;
      break;
    case URL_APPL_SMTP:
      cctx->bctx->smtp_delta.resp_2xx++;
      break;
    default:
      break;
  }
}
void stat_3xx_inc (client_context* cctx, url_appl_type protocol)
{
  switch (protocol) {
    case URL_APPL_HTTP:
      cctx->bctx->http_delta.resp_3xx++;
      break;
    case URL_APPL_HTTPS:
      cctx->bctx->https_delta.resp_3xx++;
      break;
    case URL_APPL_FTP:
      cctx->bctx->ftp_delta.resp_3xx++;
      break;
    case URL_APPL_FTPS:
      cctx->bctx->ftps_delta.resp_3xx++;
      break;
    case URL_APPL_SMTP:
      cctx->bctx->smtp_delta.resp_3xx++;
      break;
    default:
      break;
  }
}
void stat_4xx_inc (client_context* cctx, url_appl_type protocol)
{
  switch (protocol) {
    case URL_APPL_HTTP:
      cctx->bctx->http_delta.resp_4xx++;
      break;
    case URL_APPL_HTTPS:
      cctx->bctx->https_delta.resp_4xx++;
      break;
    case URL_APPL_FTP:
      cctx->bctx->ftp_delta.resp_4xx++;
      break;
    case URL_APPL_FTPS:
      cctx->bctx->ftps_delta.resp_4xx++;
      break;
    case URL_APPL_SMTP:
      cctx->bctx->smtp_delta.resp_4xx++;
      break;
    default:
      break;
  }
}
void stat_5xx_inc (client_context* cctx, url_appl_type protocol)
{
  switch (protocol) {
    case URL_APPL_HTTP:
      cctx->bctx->http_delta.resp_5xx++;
      break;
    case URL_APPL_HTTPS:
      cctx->bctx->https_delta.resp_5xx++;
      break;
    case URL_APPL_FTP:
      cctx->bctx->ftp_delta.resp_5xx++;
      break;
    case URL_APPL_FTPS:
      cctx->bctx->ftps_delta.resp_5xx++;
      break;
    case URL_APPL_SMTP:
      cctx->bctx->smtp_delta.resp_5xx++;
      break;
    default:
      break;
  }
}

void stat_6xx_inc (client_context* cctx, url_appl_type protocol)
{
  switch (protocol) {
    case URL_APPL_FTP:
      cctx->bctx->ftp_delta.resp_6xx++;
      break;
    case URL_APPL_FTPS:
      cctx->bctx->ftps_delta.resp_6xx++;
      break;
    default:
      break;
  }
}

void stat_appl_delay_add (client_context* cctx, url_appl_type protocol, unsigned long resp_timestamp)
{
  if (resp_timestamp > cctx->req_sent_timestamp)
  {
    const unsigned long delay = resp_timestamp - cctx->req_sent_timestamp;
    switch (protocol) {
      case URL_APPL_HTTP:
        cctx->bctx->http_delta.appl_delay =
            (cctx->bctx->http_delta.appl_delay * cctx->bctx->http_delta.appl_delay_points +
              delay) / ++cctx->bctx->http_delta.appl_delay_points;
        if (cctx->bctx->http_delta.max_appl_delay < delay) {
          cctx->bctx->http_delta.max_appl_delay = delay;
        }
        if (!cctx->bctx->http_delta.min_appl_delay || (delay && cctx->bctx->http_delta.min_appl_delay > delay)) {
          cctx->bctx->http_delta.min_appl_delay = delay;
        }
        break;
      case URL_APPL_HTTPS:
        cctx->bctx->https_delta.appl_delay =
            (cctx->bctx->https_delta.appl_delay * cctx->bctx->https_delta.appl_delay_points +
              delay) / ++cctx->bctx->https_delta.appl_delay_points;
        if (cctx->bctx->https_delta.max_appl_delay < delay) {
          cctx->bctx->https_delta.max_appl_delay = delay;
        }
        if (!cctx->bctx->https_delta.min_appl_delay || (delay && cctx->bctx->https_delta.min_appl_delay > delay)) {
          cctx->bctx->https_delta.min_appl_delay = delay;
        }
        break;
      case URL_APPL_FTP:
        cctx->bctx->ftp_delta.appl_delay =
            (cctx->bctx->ftp_delta.appl_delay * cctx->bctx->ftp_delta.appl_delay_points +
              delay) / ++cctx->bctx->ftp_delta.appl_delay_points;
        if (cctx->bctx->ftp_delta.max_appl_delay < delay) {
          cctx->bctx->ftp_delta.max_appl_delay = delay;
        }
        if (!cctx->bctx->ftp_delta.min_appl_delay || (delay && cctx->bctx->ftp_delta.min_appl_delay > delay)) {
          cctx->bctx->ftp_delta.min_appl_delay = delay;
        }
        break;
      case URL_APPL_FTPS:
        cctx->bctx->ftps_delta.appl_delay =
            (cctx->bctx->ftps_delta.appl_delay * cctx->bctx->ftps_delta.appl_delay_points +
              delay) / ++cctx->bctx->ftps_delta.appl_delay_points;
        if (cctx->bctx->ftps_delta.max_appl_delay < delay) {
          cctx->bctx->ftps_delta.max_appl_delay = delay;
        }
        if (!cctx->bctx->ftps_delta.min_appl_delay || (delay && cctx->bctx->ftps_delta.min_appl_delay > delay)) {
          cctx->bctx->ftps_delta.min_appl_delay = delay;
        }
        break;
      case URL_APPL_IMAP:
        cctx->bctx->imap_delta.appl_delay =
            (cctx->bctx->imap_delta.appl_delay * cctx->bctx->imap_delta.appl_delay_points +
              delay) / ++cctx->bctx->imap_delta.appl_delay_points;
        if (cctx->bctx->imap_delta.max_appl_delay < delay) {
          cctx->bctx->imap_delta.max_appl_delay = delay;
        }
        if (!cctx->bctx->imap_delta.min_appl_delay || (delay && cctx->bctx->imap_delta.min_appl_delay > delay)) {
          cctx->bctx->imap_delta.min_appl_delay = delay;
        }
        break;
      case URL_APPL_POP3:
        cctx->bctx->pop3_delta.appl_delay =
            (cctx->bctx->pop3_delta.appl_delay * cctx->bctx->pop3_delta.appl_delay_points +
              delay) / ++cctx->bctx->pop3_delta.appl_delay_points;
        if (cctx->bctx->pop3_delta.max_appl_delay < delay) {
          cctx->bctx->pop3_delta.max_appl_delay = delay;
        }
        if (!cctx->bctx->pop3_delta.min_appl_delay || (delay && cctx->bctx->pop3_delta.min_appl_delay > delay)) {
          cctx->bctx->pop3_delta.min_appl_delay = delay;
        }
        break;
      case URL_APPL_SMTP:
        cctx->bctx->smtp_delta.appl_delay =
            (cctx->bctx->smtp_delta.appl_delay * cctx->bctx->smtp_delta.appl_delay_points +
              delay) / ++cctx->bctx->smtp_delta.appl_delay_points;
        if (cctx->bctx->smtp_delta.max_appl_delay < delay) {
          cctx->bctx->smtp_delta.max_appl_delay = delay;
        }
        if (!cctx->bctx->smtp_delta.min_appl_delay || (delay && cctx->bctx->smtp_delta.min_appl_delay > delay)) {
          cctx->bctx->smtp_delta.min_appl_delay = delay;
        }
        break;
      default:
        break;
    }
  }
}
void stat_appl_delay_2xx_add (client_context* cctx, url_appl_type protocol, unsigned long resp_timestamp)
{
    switch (protocol) {
      case URL_APPL_HTTP:
        cctx->bctx->http_delta.appl_delay_2xx =
            (cctx->bctx->http_delta.appl_delay_2xx * cctx->bctx->http_delta.appl_delay_2xx_points +
             resp_timestamp - cctx->req_sent_timestamp) / ++cctx->bctx->http_delta.appl_delay_2xx_points;
        break;
      case URL_APPL_HTTPS:
        cctx->bctx->https_delta.appl_delay_2xx =
            (cctx->bctx->https_delta.appl_delay_2xx * cctx->bctx->https_delta.appl_delay_2xx_points +
                  resp_timestamp - cctx->req_sent_timestamp) / ++cctx->bctx->https_delta.appl_delay_2xx_points;
        break;
      case URL_APPL_FTP:
        cctx->bctx->ftp_delta.appl_delay_2xx =
            (cctx->bctx->ftp_delta.appl_delay_2xx * cctx->bctx->ftp_delta.appl_delay_2xx_points +
             resp_timestamp - cctx->req_sent_timestamp) / ++cctx->bctx->ftp_delta.appl_delay_2xx_points;
        break;
      case URL_APPL_FTPS:
        cctx->bctx->ftps_delta.appl_delay_2xx =
            (cctx->bctx->ftps_delta.appl_delay_2xx * cctx->bctx->ftps_delta.appl_delay_2xx_points +
             resp_timestamp - cctx->req_sent_timestamp) / ++cctx->bctx->ftps_delta.appl_delay_2xx_points;
        break;
      case URL_APPL_SMTP:
        cctx->bctx->smtp_delta.appl_delay_2xx =
            (cctx->bctx->smtp_delta.appl_delay_2xx * cctx->bctx->smtp_delta.appl_delay_2xx_points +
             resp_timestamp - cctx->req_sent_timestamp) / ++cctx->bctx->smtp_delta.appl_delay_2xx_points;
        break;
      default:
        break;
    }
}

/*
  Callback from libcurl to handle file-upload reads.
*/
static size_t
read_callback(void *ptr, size_t size, size_t nmemb, void* user_supplied)
{
    extern ssize_t pread(int, void *, size_t, off_t);

    client_context* client = user_supplied;
    batch_context* batch = client->bctx;
    url_context* url = & batch->url_ctx_array[client->url_curr_index];
    off_t* offset_ptr = & url->upload_offsets[client->client_index];
    int nread;

   /*
     Use pread instaed of fseek/fread for thread safety
   */
    if ((nread = pread(url->upload_descriptor, ptr, size * nmemb, *offset_ptr)) < 0)
    {
        fprintf(stderr, "pread returned %d\n", errno);
        return 0;
    }

    *offset_ptr += nread;
    return nread;
}

/*********************************************************
	Per-client file upload streams.
	Allows multiple clients to upload the same file, and
	cycling URLs to continually upload the same file.
*********************************************************/
/*
  Called from upload_file_parser to allocate per-client file upload streams
*/
int upload_file_streams_alloc (batch_context* batch, int url_index)
{
    url_context* url = &batch->url_ctx_array[url_index];

    if ((url->upload_offsets = (off_t*) calloc(batch->client_num_max, sizeof (off_t))) == 0) {
        fprintf(stderr, "cannot allocate upload streams\n");
        return -1;
    }

    return 0;
}

/*
  Called from setup_curl_handle_init to initialize a per-client upload file stream,
  and re-initialize the stream for a cycling url.
*/
int upload_file_stream_init (client_context* client, url_context* url)
{
    off_t* offset_ptr = & url->upload_offsets[client->client_index];
    CURL* handle = client->handle;

    if (url->upload_file == 0)
    {
        return 0;
    }

    if	(
        url->upload_file_ptr == 0 &&
        (url->upload_file_ptr = fopen(url->upload_file, "rb")) == 0
        )
    {
        return fprintf(stderr, "fopen(%s) failed, errno = %d\n", url->upload_file, errno);
    }

    url->upload_descriptor = fileno(url->upload_file_ptr);
    *offset_ptr = 0; /* re-initializes the stream for a cycling url */

    curl_easy_setopt(handle, CURLOPT_UPLOAD, 1);
    curl_easy_setopt(handle, CURLOPT_READFUNCTION, read_callback);
    curl_easy_setopt(handle, CURLOPT_READDATA, client);
    curl_easy_setopt(handle, CURLOPT_INFILESIZE, (long) url->upload_file_size);

    if (url->transfer_limit_rate)
    {
        curl_easy_setopt(handle, CURLOPT_MAX_SEND_SPEED_LARGE,(curl_off_t) url->transfer_limit_rate);
    }

    return 0;
}
