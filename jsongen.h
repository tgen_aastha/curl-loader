#ifndef __JSONGEN_H__
#define __JSONGEN_H__

#define JSON_WITH(json) char *json##_p = json; int json##_len = 0
#define JSON_START(json) do { json##_p[0] = '\0'; sprintf(json##_p, "{"); json##_len++; } while(0)
#define JSON_END(json) do { if (json##_p[json##_len-1] == '{') {sprintf(json##_p+json##_len, "}"); json##_len++;} else {sprintf(json##_p+json##_len-1, "}");} } while(0)
#define JSON_PUT_OBJECT_START(json, key) do { int __tmp_len = sprintf(json##_p+json##_len, "\"" #key "\":{"); json##_len += __tmp_len; } while(0)
#define JSON_OBJECT_START(json) do { int __tmp_len = sprintf(json##_p+json##_len, "{"); json##_len += __tmp_len; } while(0)
#define JSON_OBJECT_END(json) do { if (json##_p[json##_len-1] == '{') {sprintf(json##_p+json##_len, "},"); json##_len+=2;} else {sprintf(json##_p+json##_len-1, "},"); json##_len++;} } while(0)
#define JSON_PUT_ARRAY_START(json, key) do { int __tmp_len = sprintf(json##_p+json##_len, "\"" #key "\":["); json##_len += __tmp_len; } while(0)
#define JSON_ARRAY_START(json) do { int __tmp_len = sprintf(json##_p+json##_len, "["); json##_len += __tmp_len; } while(0)
#define JSON_ARRAY_END(json) do { if (json##_p[json##_len-1] == '[') {sprintf(json##_p+json##_len, "],"); json##_len+=2;} else {sprintf(json##_p+json##_len-1, "],"); json##_len++;} } while(0)
#define JSON_VAL(json, format, value) do { int __tmp_len = sprintf(json##_p+json##_len, format ",", (value)); json##_len += __tmp_len; } while(0)
#define JSON_INT(json, value) JSON_VAL(json, "%d", (value))
#define JSON_LONG_INT(json, value) JSON_VAL(json, "%ld", (value))
#define JSON_LONG_LONG_INT(json, value) JSON_VAL(json, "%lld", (value))
#define JSON_DOUBLE(json, value) JSON_VAL(json, "%0.3f", (value))
#define JSON_STRING(json, value) JSON_VAL(json, "\"%s\"", (value))
#define JSON_KEY(json, key) do { int __tmp_len = sprintf(json##_p+json##_len, "\"" #key "\":"); json##_len += __tmp_len; } while(0)
#define JSON_PUT_INT(json, key, value) do {JSON_KEY(json, key); JSON_INT(json, value);} while(0)
#define JSON_PUT_LONG_INT(json, key, value) do {JSON_KEY(json, key); JSON_LONG_INT(json, value);} while(0)
#define JSON_PUT_LONG_LONG_INT(json, key, value) do {JSON_KEY(json, key); JSON_LONG_LONG_INT(json, value);} while(0)
#define JSON_PUT_DOUBLE(json, key, value) do {JSON_KEY(json, key); JSON_DOUBLE(json, value);} while(0)
#define JSON_PUT_STRING(json, key, value) do {JSON_KEY(json, key); JSON_STRING(json, value);} while(0)

#endif
