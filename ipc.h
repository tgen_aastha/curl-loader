#ifndef __IPC_H__
#define __IPC_H__

#include "batch.h"

#define CLIENTS_JSON_BUFFER_SIZE 1024
#define MAX_CLIENTS_INCREASE 100
#define MAX_CLIENTS_DECREASE -100

#define CLIENT_INCREASE_KEY "client_increase"
#define MANUAL_SCALE_CONTROL_KEY "manual_scale_control"

enum
{
  MSG_TYPE_EVENT_TEST_READY = 1,
  MSG_TYPE_EVENT_STATS,
  MSG_TYPE_CMD_START_TEST,
  MSG_CHANGE_SCALE_TYPE,
  MSG_CHANGE_SCALE_NUMBER
};

enum {
  EXEC_FAIL_CODE = -2,
  PARSING_FAIL_CODE = -1,
  OK_CODE = 0
};

int ipc_is_enabled();
void ipc_send_stat(batch_context* bctx, unsigned long now, int current_clients, int max_clients);
int ipc_sync();
void ipc_clients_change(batch_context* bctx);

#endif
