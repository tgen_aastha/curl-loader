/*
*     statistics.c
*
* 2006 Copyright (c) 
* Robert Iakobashvili, <coroberti@gmail.com>
* Michael Moser,  <moser.michael@gmail.com>                 
* All rights reserved.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// must be first include
#include "fdsetsize.h"

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "batch.h"
#include "client.h"
#include "loader.h"
#include "conf.h"

#include "statistics.h"
#include "screen.h"

#include "ipc.h"

#define APPL_STR_HTTP  "HTTP  "
#define APPL_STR_HTTPS "HTTPS "
#define APPL_STR_FTP   "FTP   "
#define APPL_STR_FTPS  "FTPS  "
#define APPL_STR_IMAP  "IMAP  "
#define APPL_STR_POP3  "POP3  "
#define APPL_STR_SMTP  "SMTP  "

static void
dump_snapshot_interval_and_advance_total_statistics (FILE* fout,
                                                     batch_context* bctx,
                                                     unsigned long now_time,
                                                     int clients_total_num);

static void dump_statistics (FILE* fout,
                             unsigned long period, batch_context *bctx);

static void print_operational_statistics (FILE *opstats_file,
                                          op_stat_point*const osp_curr,
                                          op_stat_point*const osp_total,
                                          url_context* url_arr);

static void dump_http_stat_to_screen (FILE* fout,
                                 char* protocol, 
                                 stat_point_http* sd,
                                 unsigned long period);
static void dump_https_stat_to_screen (FILE* fout,
                                 char* protocol,
                                 stat_point_https* sd,
                                 unsigned long period);
static void dump_ftp_stat_to_screen (FILE* fout,
                                 char* protocol,
                                 stat_point_ftp* sd,
                                 unsigned long period);
static void dump_ftps_stat_to_screen (FILE* fout,
                                 char* protocol,
                                 stat_point_ftps* sd,
                                 unsigned long period);
static void dump_imap_stat_to_screen (FILE* fout,
                                 char* protocol,
                                 stat_point_imap* sd,
                                 unsigned long period); 
static void dump_pop3_stat_to_screen (FILE* fout,
                                 char* protocol,
                                 stat_point_pop3* sd,
                                 unsigned long period);                                  
static void dump_smtp_stat_to_screen (FILE* fout,
                                 char* protocol,
                                 stat_point_smtp* sd,
                                 unsigned long period);                                                                  

/****************************************************************************************
* Function name - stat_point_all_add_sub
*
* Description - Add counters from subbatch
****************************************************************************************/
void stat_point_all_add_sub (batch_context* bctx, batch_context* bctx_sub)
{
  stat_point_http_add (&bctx->http_delta, &bctx_sub->http_delta);
  stat_point_https_add (&bctx->https_delta, &bctx_sub->https_delta);
  stat_point_ftp_add (&bctx->ftp_delta, &bctx_sub->ftp_delta);
  stat_point_ftps_add (&bctx->ftps_delta, &bctx_sub->ftps_delta);
  stat_point_imap_add (&bctx->imap_delta, &bctx_sub->imap_delta);
  stat_point_pop3_add (&bctx->pop3_delta, &bctx_sub->pop3_delta);
  stat_point_smtp_add (&bctx->smtp_delta, &bctx_sub->smtp_delta);
}

/****************************************************************************************
* Function name - stat_point_all_acc
*
* Description - Add counters from subbatch
****************************************************************************************/
void stat_point_all_acc (batch_context* bctx)
{
  stat_point_http_add (&bctx->http_total, &bctx->http_delta);
  stat_point_https_add (&bctx->https_total, &bctx->https_delta);
  stat_point_ftp_add (&bctx->ftp_total, &bctx->ftp_delta);
  stat_point_ftps_add (&bctx->ftps_total, &bctx->ftps_delta);
  stat_point_imap_add (&bctx->imap_total, &bctx->imap_delta);
  stat_point_pop3_add (&bctx->pop3_total, &bctx->pop3_delta);
  stat_point_smtp_add (&bctx->smtp_total, &bctx->smtp_delta);
}


/****************************************************************************************
* Function name - stat_point_http_add
*
* Description - Adds counters of one stat_point_http object to another
* Input -       *left  -pointer to the stat_point_http, where counter will be added
*               *right -pointer to the stat_point_http, which counter will be
*                       added to the <left>
* Return Code/Output - None
****************************************************************************************/
void stat_point_http_add (stat_point_http* left, stat_point_http* right)
{
  if (!left || !right)
    return;

  left->data_in += right->data_in;
  left->data_out += right->data_out;

  left->requests += right->requests;
  left->connections += right->connections;

  left->resp_1xx += right->resp_1xx;
  left->resp_2xx += right->resp_2xx;
  left->resp_3xx += right->resp_3xx;
  left->resp_4xx += right->resp_4xx;
  left->resp_5xx += right->resp_5xx;
  left->other_errs += right->other_errs;
  left->url_timeout_errs += right->url_timeout_errs;

  int i;
  for (i=0; i<CURL_LAST; i++) {
    left->errs_by_type[i] += right->errs_by_type[i];
  }

  const int total_points = left->appl_delay_points + right->appl_delay_points;

  if (total_points > 0) {
    left->appl_delay = (left->appl_delay * left->appl_delay_points  +
                right->appl_delay * right->appl_delay_points) / total_points;
    if (left->max_appl_delay < right->max_appl_delay) {
      left->max_appl_delay = right->max_appl_delay;
    }
    if (!left->min_appl_delay || (right->min_appl_delay && left->min_appl_delay > right->min_appl_delay)) {
      left->min_appl_delay = right->min_appl_delay;
    }

    left->appl_delay_points = total_points;
  }
  else {
      left->appl_delay = 0;
      left->max_appl_delay = 0;
      left->min_appl_delay = 0;
  }

  const int total_points_2xx = left->appl_delay_2xx_points + right->appl_delay_2xx_points;

  if (total_points_2xx > 0)
    {
      left->appl_delay_2xx = (left->appl_delay_2xx * left->appl_delay_2xx_points  +
                 right->appl_delay_2xx * right->appl_delay_2xx_points) / total_points_2xx;
      left->appl_delay_2xx_points = total_points_2xx;
    }
  else
      left->appl_delay_2xx = 0;
}

/****************************************************************************************
* Function name - stat_point_https_add
*
* Description - Adds counters of one stat_point_https object to another
* Input -       *left  -pointer to the stat_point_https, where counter will be added
*               *right -pointer to the stat_point_https, which counter will be
*                       added to the <left>
* Return Code/Output - None
****************************************************************************************/
void stat_point_https_add (stat_point_https* left, stat_point_https* right)
{
  if (!left || !right)
    return;

  left->data_in += right->data_in;
  left->data_out += right->data_out;

  left->requests += right->requests;
  left->connections += right->connections;

  left->resp_1xx += right->resp_1xx;
  left->resp_2xx += right->resp_2xx;
  left->resp_3xx += right->resp_3xx;
  left->resp_4xx += right->resp_4xx;
  left->resp_5xx += right->resp_5xx;
  left->other_errs += right->other_errs;
  left->url_timeout_errs += right->url_timeout_errs;

  int i;
  for (i=0; i<CURL_LAST; i++) {
    left->errs_by_type[i] += right->errs_by_type[i];
  }

  const int total_points = left->appl_delay_points + right->appl_delay_points;

  if (total_points > 0) {
    left->appl_delay = (left->appl_delay * left->appl_delay_points  +
                right->appl_delay * right->appl_delay_points) / total_points;
    if (left->max_appl_delay < right->max_appl_delay) {
      left->max_appl_delay = right->max_appl_delay;
    }
    if (!left->min_appl_delay || (right->min_appl_delay && left->min_appl_delay > right->min_appl_delay)) {
      left->min_appl_delay = right->min_appl_delay;
    }

    left->appl_delay_points = total_points;
  }
  else {
      left->appl_delay = 0;
      left->max_appl_delay = 0;
      left->min_appl_delay = 0;
  }

   const int total_points_2xx = left->appl_delay_2xx_points + right->appl_delay_2xx_points;

  if (total_points_2xx > 0)
    {
      left->appl_delay_2xx = (left->appl_delay_2xx * left->appl_delay_2xx_points  +
                 right->appl_delay_2xx * right->appl_delay_2xx_points) / total_points_2xx;
      left->appl_delay_2xx_points = total_points_2xx;
    }
  else
      left->appl_delay_2xx = 0;
}

/****************************************************************************************
* Function name - stat_point_ftp_add
*
* Description - Adds counters of one stat_point_ftp object to another
* Input -       *left  -pointer to the stat_point_ftp, where counter will be added
*               *right -pointer to the stat_point_ftp, which counter will be
*                       added to the <left>
* Return Code/Output - None
****************************************************************************************/
void stat_point_ftp_add (stat_point_ftp* left, stat_point_ftp* right)
{
  if (!left || !right)
    return;

  left->data_in += right->data_in;
  left->data_out += right->data_out;

  left->requests += right->requests;
  left->connections += right->connections;

  left->resp_1xx += right->resp_1xx;
  left->resp_2xx += right->resp_2xx;
  left->resp_3xx += right->resp_3xx;
  left->resp_4xx += right->resp_4xx;
  left->resp_5xx += right->resp_5xx;
  left->other_errs += right->other_errs;
  left->url_timeout_errs += right->url_timeout_errs;

  int i;
  for (i=0; i<CURL_LAST; i++) {
    left->errs_by_type[i] += right->errs_by_type[i];
  }

  const int total_points = left->appl_delay_points + right->appl_delay_points;

  if (total_points > 0) {
    left->appl_delay = (left->appl_delay * left->appl_delay_points  +
                right->appl_delay * right->appl_delay_points) / total_points;
    if (left->max_appl_delay < right->max_appl_delay) {
      left->max_appl_delay = right->max_appl_delay;
    }
    if (!left->min_appl_delay || (right->min_appl_delay && left->min_appl_delay > right->min_appl_delay)) {
      left->min_appl_delay = right->min_appl_delay;
    }

    left->appl_delay_points = total_points;
  }
  else {
      left->appl_delay = 0;
      left->max_appl_delay = 0;
      left->min_appl_delay = 0;
  }

   const int total_points_2xx = left->appl_delay_2xx_points + right->appl_delay_2xx_points;

  if (total_points_2xx > 0)
    {
      left->appl_delay_2xx = (left->appl_delay_2xx * left->appl_delay_2xx_points  +
                 right->appl_delay_2xx * right->appl_delay_2xx_points) / total_points_2xx;
      left->appl_delay_2xx_points = total_points_2xx;
    }
  else
      left->appl_delay_2xx = 0;
}

/****************************************************************************************
* Function name - stat_point_ftps_add
*
* Description - Adds counters of one stat_point_ftps object to another
* Input -       *left  -pointer to the stat_point_ftps, where counter will be added
*               *right -pointer to the stat_point_ftps, which counter will be
*                       added to the <left>
* Return Code/Output - None
****************************************************************************************/
void stat_point_ftps_add (stat_point_ftps* left, stat_point_ftps* right)
{
  if (!left || !right)
    return;

  left->data_in += right->data_in;
  left->data_out += right->data_out;

  left->requests += right->requests;
  left->connections += right->connections;

  left->resp_1xx += right->resp_1xx;
  left->resp_2xx += right->resp_2xx;
  left->resp_3xx += right->resp_3xx;
  left->resp_4xx += right->resp_4xx;
  left->resp_5xx += right->resp_5xx;
  left->other_errs += right->other_errs;
  left->url_timeout_errs += right->url_timeout_errs;

  int i;
  for (i=0; i<CURL_LAST; i++) {
    left->errs_by_type[i] += right->errs_by_type[i];
  }

  const int total_points = left->appl_delay_points + right->appl_delay_points;

  if (total_points > 0) {
    left->appl_delay = (left->appl_delay * left->appl_delay_points  +
                right->appl_delay * right->appl_delay_points) / total_points;
    if (left->max_appl_delay < right->max_appl_delay) {
      left->max_appl_delay = right->max_appl_delay;
    }
    if (!left->min_appl_delay || (right->min_appl_delay && left->min_appl_delay > right->min_appl_delay)) {
      left->min_appl_delay = right->min_appl_delay;
    }

    left->appl_delay_points = total_points;
  }
  else {
      left->appl_delay = 0;
      left->max_appl_delay = 0;
      left->min_appl_delay = 0;
  }

   const int total_points_2xx = left->appl_delay_2xx_points + right->appl_delay_2xx_points;

  if (total_points_2xx > 0)
    {
      left->appl_delay_2xx = (left->appl_delay_2xx * left->appl_delay_2xx_points  +
                 right->appl_delay_2xx * right->appl_delay_2xx_points) / total_points_2xx;
      left->appl_delay_2xx_points = total_points_2xx;
    }
  else
      left->appl_delay_2xx = 0;
}

/****************************************************************************************
* Function name - stat_point_imap_add
*
* Description - Adds counters of one stat_point_imap object to another
* Input -       *left  -pointer to the stat_point_imap, where counter will be added
*               *right -pointer to the stat_point_imap, which counter will be
*                       added to the <left>
* Return Code/Output - None
****************************************************************************************/
void stat_point_imap_add (stat_point_imap* left, stat_point_imap* right)
{
  if (!left || !right)
    return;

  left->data_in += right->data_in;
  left->data_out += right->data_out;

  left->requests += right->requests;
  left->connections += right->connections;

  left->other_errs += right->other_errs;
  left->url_timeout_errs += right->url_timeout_errs;

  int i;
  for (i=0; i<CURL_LAST; i++) {
    left->errs_by_type[i] += right->errs_by_type[i];
  }

  const int total_points = left->appl_delay_points + right->appl_delay_points;

  if (total_points > 0) {
    left->appl_delay = (left->appl_delay * left->appl_delay_points  +
                right->appl_delay * right->appl_delay_points) / total_points;
    if (left->max_appl_delay < right->max_appl_delay) {
      left->max_appl_delay = right->max_appl_delay;
    }
    if (!left->min_appl_delay || (right->min_appl_delay && left->min_appl_delay > right->min_appl_delay)) {
      left->min_appl_delay = right->min_appl_delay;
    }

    left->appl_delay_points = total_points;
  }
  else {
      left->appl_delay = 0;
      left->max_appl_delay = 0;
      left->min_appl_delay = 0;
  }
}

/****************************************************************************************
* Function name - stat_point_pop3_add
*
* Description - Adds counters of one stat_point_pop3 object to another
* Input -       *left  -pointer to the stat_point_pop3, where counter will be added
*               *right -pointer to the stat_point_pop3, which counter will be
*                       added to the <left>
* Return Code/Output - None
****************************************************************************************/
void stat_point_pop3_add (stat_point_pop3* left, stat_point_pop3* right)
{
  if (!left || !right)
    return;

  left->data_in += right->data_in;
  left->data_out += right->data_out;

  left->requests += right->requests;
  left->connections += right->connections;

  left->other_errs += right->other_errs;
  left->url_timeout_errs += right->url_timeout_errs;

  int i;
  for (i=0; i<CURL_LAST; i++) {
    left->errs_by_type[i] += right->errs_by_type[i];
  }

  const int total_points = left->appl_delay_points + right->appl_delay_points;

  if (total_points > 0) {
    left->appl_delay = (left->appl_delay * left->appl_delay_points  +
                right->appl_delay * right->appl_delay_points) / total_points;
    if (left->max_appl_delay < right->max_appl_delay) {
      left->max_appl_delay = right->max_appl_delay;
    }
    if (!left->min_appl_delay || (right->min_appl_delay && left->min_appl_delay > right->min_appl_delay)) {
      left->min_appl_delay = right->min_appl_delay;
    }

    left->appl_delay_points = total_points;
  }
  else {
      left->appl_delay = 0;
      left->max_appl_delay = 0;
      left->min_appl_delay = 0;
  }
}

/****************************************************************************************
* Function name - stat_point_smtp_add
*
* Description - Adds counters of one stat_point_smtp object to another
* Input -       *left  -pointer to the stat_point_smtp, where counter will be added
*               *right -pointer to the stat_point_smtp, which counter will be
*                       added to the <left>
* Return Code/Output - None
****************************************************************************************/
void stat_point_smtp_add (stat_point_smtp* left, stat_point_smtp* right)
{
  if (!left || !right)
    return;

  left->data_in += right->data_in;
  left->data_out += right->data_out;

  left->requests += right->requests;
  left->connections += right->connections;

  left->resp_2xx += right->resp_2xx;
  left->resp_3xx += right->resp_3xx;
  left->resp_4xx += right->resp_4xx;
  left->resp_5xx += right->resp_5xx;
  left->other_errs += right->other_errs;
  left->url_timeout_errs += right->url_timeout_errs;

  int i;
  for (i=0; i<CURL_LAST; i++) {
    left->errs_by_type[i] += right->errs_by_type[i];
  }

  const int total_points = left->appl_delay_points + right->appl_delay_points;

  if (total_points > 0) {
    left->appl_delay = (left->appl_delay * left->appl_delay_points  +
                right->appl_delay * right->appl_delay_points) / total_points;
    if (left->max_appl_delay < right->max_appl_delay) {
      left->max_appl_delay = right->max_appl_delay;
    }
    if (!left->min_appl_delay || (right->min_appl_delay && left->min_appl_delay > right->min_appl_delay)) {
      left->min_appl_delay = right->min_appl_delay;
    }

    left->appl_delay_points = total_points;
  }
  else {
      left->appl_delay = 0;
      left->max_appl_delay = 0;
      left->min_appl_delay = 0;
  }

  const int total_points_2xx = left->appl_delay_2xx_points + right->appl_delay_2xx_points;

  if (total_points_2xx > 0)
    {
      left->appl_delay_2xx = (left->appl_delay_2xx * left->appl_delay_2xx_points  +
                 right->appl_delay_2xx * right->appl_delay_2xx_points) / total_points_2xx;
      left->appl_delay_2xx_points = total_points_2xx;
    }
  else {
      left->appl_delay_2xx = 0;
  }
}
/****************************************************************************************
* Function name - stat_point_all_reset
*
* Description - Nulls counters for all delta statistics
****************************************************************************************/
void stat_point_all_reset (batch_context *bctx)
{
  stat_point_http_reset(&bctx->http_delta);
  stat_point_https_reset(&bctx->https_delta);
  stat_point_ftp_reset(&bctx->ftp_delta);
  stat_point_ftps_reset(&bctx->ftps_delta);
  stat_point_imap_reset(&bctx->imap_delta);
  stat_point_pop3_reset(&bctx->pop3_delta);
  stat_point_smtp_reset(&bctx->smtp_delta);
}

/****************************************************************************************
* Function name - stat_point_http_reset
*
* Description - Nulls counters of a stat_point_http structure
*
* Input -       *point -  pointer to the stat_point_http
* Return Code/Output - None
****************************************************************************************/
void stat_point_http_reset (stat_point_http* p)
{
  if (!p)
    return;

  p->data_in = p->data_out = 0;
  p->requests = p->connections = p->resp_1xx = p->resp_2xx = p->resp_3xx = p->resp_4xx =
      p->resp_5xx = p->other_errs = p->url_timeout_errs = 0;

  memset(p->errs_by_type, 0, sizeof(p->errs_by_type));

  p->appl_delay_points = p->appl_delay_2xx_points = 0;
  p->appl_delay = p->appl_delay_2xx = p->max_appl_delay = p->min_appl_delay = 0;

}

/****************************************************************************************
* Function name - stat_point_https_reset
*
* Description - Nulls counters of a stat_point_https structure
*
* Input -       *point -  pointer to the stat_point_https
* Return Code/Output - None
****************************************************************************************/
void stat_point_https_reset (stat_point_https* p)
{
  if (!p)
    return;

  p->data_in = p->data_out = 0;
  p->requests = p->connections = p->resp_1xx = p->resp_2xx = p->resp_3xx = p->resp_4xx =
      p->resp_5xx = p->other_errs = p->url_timeout_errs = 0;

  memset(p->errs_by_type, 0, sizeof(p->errs_by_type));

  p->appl_delay_points = p->appl_delay_2xx_points = 0;
  p->appl_delay = p->appl_delay_2xx = p->max_appl_delay = p->min_appl_delay = 0;

}

/****************************************************************************************
* Function name - stat_point_ftp_reset
*
* Description - Nulls counters of a stat_point_ftp structure
*
* Input -       *point -  pointer to the stat_point_ftp
* Return Code/Output - None
****************************************************************************************/
void stat_point_ftp_reset (stat_point_ftp* p)
{
  if (!p)
    return;

  p->data_in = p->data_out = 0;
  p->requests = p->connections = p->resp_1xx = p->resp_2xx = p->resp_3xx = p->resp_4xx =
      p->resp_5xx = p->other_errs = p->url_timeout_errs = 0;

  memset(p->errs_by_type, 0, sizeof(p->errs_by_type));

  p->appl_delay_points = p->appl_delay_2xx_points = 0;
  p->appl_delay = p->appl_delay_2xx = p->max_appl_delay = p->min_appl_delay = 0;

}

/****************************************************************************************
* Function name - stat_point_ftps_reset
*
* Description - Nulls counters of a stat_point_ftps structure
*
* Input -       *point -  pointer to the stat_point_ftps
* Return Code/Output - None
****************************************************************************************/
void stat_point_ftps_reset (stat_point_ftps* p)
{
  if (!p)
    return;

  p->data_in = p->data_out = 0;
  p->requests = p->connections = p->resp_1xx = p->resp_2xx = p->resp_3xx = p->resp_4xx =
      p->resp_5xx = p->other_errs = p->url_timeout_errs = 0;

  memset(p->errs_by_type, 0, sizeof(p->errs_by_type));

  p->appl_delay_points = p->appl_delay_2xx_points = 0;
  p->appl_delay = p->appl_delay_2xx = p->max_appl_delay = p->min_appl_delay = 0;

}

/****************************************************************************************
* Function name - stat_point_imap_reset
*
* Description - Nulls counters of a stat_point_imap structure
*
* Input -       *point -  pointer to the stat_point_imap
* Return Code/Output - None
****************************************************************************************/
void stat_point_imap_reset (stat_point_imap* p)
{
  if (!p)
    return;

  p->data_in = 0;
  p->data_out = 0;   //TODO: investigate why does it get segfault
  p->requests = p->connections = p->other_errs = p->url_timeout_errs = 0;

  memset(p->errs_by_type, 0, sizeof(p->errs_by_type));

  p->appl_delay_points = 0;
  p->appl_delay = p->max_appl_delay = p->min_appl_delay = 0;
}

/****************************************************************************************
* Function name - stat_point_pop3_reset
*
* Description - Nulls counters of a stat_point_pop3 structure
*
* Input -       *point -  pointer to the stat_point_pop3
* Return Code/Output - None
****************************************************************************************/
void stat_point_pop3_reset (stat_point_pop3* p)
{
  if (!p)
    return;

  p->data_in = 0;
  p->data_out = 0;   //TODO: investigate why does it get segfault
  p->requests = p->connections = p->other_errs = p->url_timeout_errs = 0;

  memset(p->errs_by_type, 0, sizeof(p->errs_by_type));

  p->appl_delay_points = 0;
  p->appl_delay = p->max_appl_delay = p->min_appl_delay = 0;
}

/****************************************************************************************
* Function name - stat_point_smtp_reset
*
* Description - Nulls counters of a stat_point_smtp structure
*
* Input -       *point -  pointer to the stat_point_smtp
* Return Code/Output - None
****************************************************************************************/
void stat_point_smtp_reset (stat_point_smtp* p)
{
  if (!p)
    return;

  p->data_in = p->data_out = 0;
  p->requests = p->connections = p->resp_2xx = p->resp_3xx = p->resp_4xx =
      p->resp_5xx = p->other_errs = p->url_timeout_errs = 0;

  memset(p->errs_by_type, 0, sizeof(p->errs_by_type));

  p->appl_delay_points = p->appl_delay_2xx_points = 0;
  p->appl_delay = p->appl_delay_2xx = p->max_appl_delay = p->min_appl_delay = 0;
}
/****************************************************************************************
* Function name - op_stat_point_add
*
* Description - Adds counters of one op_stat_point object to another
* Input -       *left  -  pointer to the op_stat_point, where counter will be added
*               *right -  pointer to the op_stat_point, which counter will be added 
*                         to the <left>
* Return Code/Output - None
****************************************************************************************/
void op_stat_point_add (op_stat_point* left, op_stat_point* right)
{
  size_t i;
  
  if (!left || !right)
    return;

  if (left->url_num != right->url_num)
    return;
  
  for ( i = 0; i < left->url_num; i++)
    {
      left->url_ok[i] += right->url_ok[i];
      left->url_failed[i] += right->url_failed[i];
      left->url_timeouted[i] += right->url_timeouted[i];
    }
  
  left->call_init_count += right->call_init_count;
}

/****************************************************************************************
* Function name - op_stat_point_reset
*
* Description - Nulls counters of an op_stat_point structure
* Input -       *point -  pointer to the op_stat_point
* Return Code/Output - None
****************************************************************************************/
void op_stat_point_reset (op_stat_point* point)
{
  if (!point)
    return;

  if (point->url_num)
    {
      size_t i;
      for ( i = 0; i < point->url_num; i++)
        {
          point->url_ok[i] = point->url_failed[i] = point->url_timeouted[i] = 0;
        }
    }
    /* Don't null point->url_num ! */

   point->call_init_count = 0;
}

void reset_snapshot_interval_statistics(batch_context* bctx, unsigned long now)
{
  op_stat_point_reset (&bctx->op_delta);
  stat_point_all_reset (bctx);
  bctx->last_measure = now;
}

/****************************************************************************************
* Function name -  op_stat_point_release
*
* Description - Releases memory allocated by op_stat_point_init ()
* Input -       *point -  pointer to the op_stat_point, where counter will be added
* Return Code/Output - None
****************************************************************************************/
void op_stat_point_release (op_stat_point* point)
{
  if (point->url_ok)
    {
      free (point->url_ok);
      point->url_ok = NULL;
    }

  if (point->url_failed)
    {
      free (point->url_failed);
      point->url_failed = NULL;
    }

    if (point->url_timeouted)
    {
      free (point->url_timeouted);
      point->url_timeouted = NULL;
    }

  memset (point, 0, sizeof (op_stat_point));
}

/****************************************************************************************
* Function name - op_stat_point_init
*
* Description - Initializes an allocated op_stat_point by allocating relevant pointer 
* 		fields for counters
*
* Input -       *point  - pointer to the op_stat_point, where counter will be added
*               url_num - number of urls
*
* Return Code/Output - None
****************************************************************************************/
int op_stat_point_init (op_stat_point* point, size_t url_num)
{
  if (! point)
    return -1;

   if (url_num)
    { 
      if (!(point->url_ok = calloc (url_num, sizeof (unsigned long))) ||
          !(point->url_failed = calloc (url_num, sizeof (unsigned long))) ||
          !(point->url_timeouted = calloc (url_num, sizeof (unsigned long)))
          )
        {
          goto allocation_failed;
        }
      else
        point->url_num = url_num;
    }

  point->call_init_count = 0;

  return 0;

 allocation_failed:
  fprintf(stderr, "%s - calloc () failed with errno %d.\n", 
              __func__, errno);
  return -1;
}

/****************************************************************************************
* Function name -  op_stat_update
*
* Description - Updates operation statistics using information from client context
*
* Input -       *point           - pointer to the op_stat_point, where counters to be updated
*               current_state    - current state of a client
*               prev_state       - previous state of a client
*               current_url_index- current url index of a the client
*               prev_url_index   - previous url index of a the client
*
* Return Code/Output - None
****************************************************************************************/
void op_stat_update (op_stat_point* op_stat, 
                     int current_state, 
                     int prev_state,
                     size_t current_url_index,
                     size_t prev_url_index)
{
  (void) current_url_index;
  

  if (!op_stat)
    return;

  if (prev_state == CSTATE_URLS)
    {
      (current_state == CSTATE_ERROR) ? op_stat-> url_failed[prev_url_index]++ :
        op_stat->url_ok[prev_url_index]++;
    }
  
  return;
}

void op_stat_timeouted (op_stat_point* op_stat, size_t url_index)
{
    if (!op_stat)
    return;

    op_stat-> url_timeouted[url_index]++;
}

void op_stat_call_init_count_inc (op_stat_point* op_stat)
{
  op_stat->call_init_count++;
}

/****************************************************************************************
* Function name - get_tick_count
*
* Description - Delivers timestamp in milliseconds.
* 
* Return Code/Output - timestamp in milliseconds or -1 on errors
****************************************************************************************/
unsigned long get_tick_count ()
{
  struct timeval  tval;

  if (gettimeofday (&tval, NULL) == -1)
    {
      fprintf(stderr, "%s - gettimeofday () failed with errno %d.\n", 
              __func__, errno);
      exit (1);
    }
  return tval.tv_sec * 1000 + (tval.tv_usec / 1000);
}


/****************************************************************************************
* Function name - dump_final_statistics
*
* Description - Dumps final statistics counters to stdout and statistics file using 
*               print_snapshot_interval_statistics and print_statistics_* functions.
*
* Input -       *cctx - pointer to client context, where the decision to 
*                       complete loading (and dump) has been made. 
* Return Code/Output - None
****************************************************************************************/
void dump_final_statistics (FILE* fout,
                            client_context* cctx)
{
  int i;
  batch_context* bctx = cctx->bctx;
  unsigned long now = get_tick_count();

  for (i = 0; i <= threads_subbatches_num; i++)
    {
      if (i)
        {
          stat_point_all_add_sub (bctx, bctx + i);
          /* Other threads statistics - reset just after collecting */
          stat_point_all_reset (bctx + i);
        }
    }
  
  print_snapshot_interval_statistics (fout,
                                      now - bctx->last_measure,
                                      bctx);

  stat_point_all_acc(bctx);
    
  fprintf(fout,"\n==================================================="
          "====================================\n");
  fprintf(fout,"End of the test for batch: %-10.10s\n", bctx->batch_name); 
  fprintf(fout,"======================================================"
          "=================================\n\n");
  
  /* Looks like a bug */
  now = get_tick_count();

  const int seconds_run = (int)(now - bctx->start_time)/ 1000;
  if (!seconds_run)
    return;
  
  fprintf(fout,"\nTest total duration was %d seconds and CAPS average %ld:\n", 
          seconds_run, bctx->op_total.call_init_count / seconds_run);

  dump_statistics (fout,
                   seconds_run, 
                   bctx);


  int total_current_clients = 0;
  int total_client_num_max = 0;

  for (i = 0; i <= threads_subbatches_num; i++)
    {
      total_current_clients +=
        pending_active_and_waiting_clients_num_stat (bctx + i);
      total_client_num_max += (bctx + i)->client_num_max;
      if (i)
        {
          op_stat_point_add (&bctx->op_delta, &(bctx + i)->op_delta );
          
          /* Other threads operational statistics - reset just after collecting */
          op_stat_point_reset (&(bctx + i)->op_delta);
        }
    }
  op_stat_point_add (&bctx->op_total, &bctx->op_delta);
  
//  print_operational_statistics (bctx->opstats_file,
//                                &bctx->op_delta,
//                                &bctx->op_total,
//                                bctx->url_ctx_array);

  if (ipc_is_enabled()) {
    /* Send final record */
    /* TODO: bctx->http_delta probably invalid */
    ipc_send_stat(bctx, now, total_current_clients, total_client_num_max);
  }
}

/******
* Function name - ascii_time
*
* Description - evaluate current time in ascii
*
* Input -       *tbuf - pointer to time buffer
* Return -      tbuf filled with time
******/

char *ascii_time (char *tbuf)
{
  time_t timeb;

  (void)time(&timeb);
  return ctime_r(&timeb,tbuf);
}

/****************************************************************************************
* Function name - dump_snapshot_interval
*
* Description - Dumps summary statistics since the start of load
* Input -       *bctx - pointer to batch structure
*               now   - current time in msec since epoch
* Return Code/Output - None
****************************************************************************************/
void dump_snapshot_interval (FILE* fout, batch_context* bctx, unsigned long now)
{
  if (!stop_loading)
    {
      fprintf(stdout, "\033[2J");
    }

  int i;
  int total_current_clients = 0;

  for (i = 0; i <= threads_subbatches_num; i++)
    {
      total_current_clients +=
        pending_active_and_waiting_clients_num_stat (bctx + i);
    }

  dump_snapshot_interval_and_advance_total_statistics (fout,
                                                       bctx,
                                                       now,
                                                       total_current_clients);

  int seconds_run = (int)(now - bctx->start_time)/ 1000;
  if (!seconds_run)
    {
      seconds_run = 1;
    }

  fprintf(fout,"--------------------------------------------------------------------------------\n");

  fprintf(fout,"Summary stats (runs:%d secs, CAPS-average:%ld):\n", 
          seconds_run, bctx->op_total.call_init_count / seconds_run);

  dump_statistics (fout,
                   seconds_run, 
                   bctx);

  fprintf(fout,"============================================================"
          "=====================\n");

  long total_clients_rampup_inc = 0;
  int total_client_num_max = 0;
  
  for (i = 0; i <= threads_subbatches_num; i++)
    {
      total_clients_rampup_inc += (bctx + i)->clients_rampup_inc;
      total_client_num_max += (bctx + i)->client_num_max;
    }

  if (ipc_is_enabled()) {
    ipc_send_stat(bctx, now, total_current_clients, total_client_num_max);
  }

  reset_snapshot_interval_statistics(bctx, now);

  if (bctx->do_client_num_gradual_increase && 
      (bctx->stop_client_num_gradual_increase == 0))
    {
      fprintf(fout," Automatic: adding %ld clients/sec. Stop inc and manual [M].\n",
              total_clients_rampup_inc);
    }
  else
    {
      const int current_clients =
        pending_active_and_waiting_clients_num_stat (bctx);

      fprintf(fout," Manual: clients:max[%d],curr[%d]. Inc num: [+|*].",
              total_client_num_max, total_current_clients);

      if (bctx->stop_client_num_gradual_increase && 
          bctx->clients_rampup_inc &&
          current_clients < bctx->client_num_max)
        {
          fprintf(fout," Automatic: [A].\n");
        }
      else
        {
          fprintf(fout,"\n");
        }
    }

  fprintf(fout,"============================================================"
          "=====================\n");
  fflush (fout);
}

/****************************************************************************************
* Function name - print_snapshot_interval_statistics
*
* Description - Outputs latest snapshot interval statistics. 
*
* Input -       period - latest time period in milliseconds
*               *http  - pointer to the HTTP collected statistics to output
*               *https - pointer to the HTTPS collected statistics to output
* Return Code/Output - None
****************************************************************************************/
void print_snapshot_interval_statistics (FILE* fout,
                                         unsigned long period,  
                                         batch_context *bctx)
{
  period /= 1000;
  if (period == 0)
    {
      period = 1;
    }

  dump_http_stat_to_screen (fout, APPL_STR_HTTP, &bctx->http_delta, period);
  dump_https_stat_to_screen (fout, APPL_STR_HTTPS, &bctx->https_delta, period);
  dump_ftp_stat_to_screen (fout, APPL_STR_FTP, &bctx->ftp_delta, period);
  dump_ftps_stat_to_screen (fout, APPL_STR_FTPS, &bctx->ftps_delta, period);
  dump_imap_stat_to_screen (fout, APPL_STR_IMAP, &bctx->imap_delta, period);
  dump_pop3_stat_to_screen (fout, APPL_STR_POP3, &bctx->pop3_delta, period);
  dump_smtp_stat_to_screen (fout, APPL_STR_SMTP, &bctx->smtp_delta, period);
}


/****************************************************************************************
* Function name - dump_snapshot_interval_and_advance_total_statistics
*
* Description - Dumps snapshot_interval statistics for the latest loading time 
*               period and adds this statistics to the total loading counters. 
*
* Input -       *bctx    - pointer to batch context
*               now_time - current time in msec since the epoch
*
* Return Code/Output - None
****************************************************************************************/
void dump_snapshot_interval_and_advance_total_statistics (FILE* fout,
                                                          batch_context* bctx,
                                                          unsigned long now_time,
                                                          int clients_total_num)
{
  int i;
  const unsigned long delta_t = now_time - bctx->last_measure; 
  const unsigned long delta_time = delta_t ? delta_t : 1;

  if (stop_loading)
    {
      dump_final_statistics (fout,
                             bctx->cctx_array);
      screen_release ();
      exit (1); 
    }

  fprintf(fout,"============  loading batch is: %-10.10s ===================="
          "==================\n",
          bctx->batch_name);

  /*Collect the operational statistics*/

  for (i = 0; i <= threads_subbatches_num; i++)
    {
      if (i)
        {
          op_stat_point_add (&bctx->op_delta, &(bctx + i)->op_delta );

          /* Other threads operational statistics - reset just after collecting */
          op_stat_point_reset (&(bctx + i)->op_delta);
        }
    }

  op_stat_point_add (&bctx->op_total, &bctx->op_delta );

//  print_operational_statistics (bctx->opstats_file,
//                                &bctx->op_delta,
//                                &bctx->op_total,
//                                bctx->url_ctx_array);


  fprintf(fout, "--------------------------------------------------------------------------------\n");

  fprintf(fout, "Interval stats (latest:%ld sec, clients:%d, CAPS-curr:%ld):\n",
          (unsigned long ) delta_time/1000, clients_total_num,
          bctx->op_delta.call_init_count* 1000/delta_time);

  for (i = 0; i <= threads_subbatches_num; i++)
    {
      if (i)
        {
          stat_point_all_add_sub (bctx, bctx + i);
          /* Other threads statistics - reset just after collecting */
          stat_point_all_reset (bctx + i);
        }
    }

  stat_point_all_acc (bctx);

  print_snapshot_interval_statistics(fout,
                                     delta_time, 
                                     bctx);
}

/****************************************************************************************
* Function name - dump_statistics
*
* Description - Dumps statistics to screen
*
* Input -       period - time interval of the statistics collection in msecs. 
*               *http  - pointer to stat_point structure with HTTP/FTP counters collection
*               *https - pointer to stat_point structure with HTTPS/FTPS counters collection
*
* Return Code/Output - None
****************************************************************************************/
static void dump_statistics (FILE* fout,
                             unsigned long period,
                             batch_context *bctx)
{
  if (period == 0)
    {
      fprintf(stderr,
              "%s - less than 1 second duration test without statistics.\n",
              __func__);
      return;
    }
  
  dump_http_stat_to_screen (fout, APPL_STR_HTTP, &bctx->http_total, period);
  dump_https_stat_to_screen (fout, APPL_STR_HTTPS, &bctx->https_total, period);
  dump_ftp_stat_to_screen (fout, APPL_STR_FTP, &bctx->ftp_total, period);
  dump_ftps_stat_to_screen (fout, APPL_STR_FTPS, &bctx->ftps_total, period);
  dump_imap_stat_to_screen (fout, APPL_STR_IMAP, &bctx->imap_total, period);
  dump_pop3_stat_to_screen (fout, APPL_STR_POP3, &bctx->pop3_total, period);
  dump_smtp_stat_to_screen (fout, APPL_STR_SMTP, &bctx->smtp_total, period);
}


/****************************************************************************************
* Function name - dump_stat_to_screen
*
* Description - Dumps statistics to screen
*
* Input -       *protocol - name of the application/protocol
*               *sd       - pointer to statistics data with statistics counters collection
*               period    - time interval of the statistics collection in msecs. 
*
* Return Code/Output - None
****************************************************************************************/
static void dump_http_stat_to_screen (FILE* fout,
                                 char* protocol,
                                 stat_point_http* sd,
                                 unsigned long period)
{
  fprintf(fout, "%sReq:%ld,1xx:%ld,2xx:%ld,3xx:%ld,4xx:%ld,5xx:%ld,Err:%ld,T-Err:%ld,"
          "D:%ldms,D-2xx:%ldms,Ti:%lldB/s,To:%lldB/s\n",
          protocol, sd->requests, sd->resp_1xx, sd->resp_2xx, sd->resp_3xx,
          sd->resp_4xx, sd->resp_5xx, sd->other_errs, sd->url_timeout_errs, sd->appl_delay, 
          sd->appl_delay_2xx, sd->data_in/period, sd->data_out/period);

    //fprintf (fout, "Appl-Delay-Points %d, Appl-Delay-2xx-Points %d \n", 
  //         sd->appl_delay_points, sd->appl_delay_2xx_points);
}
static void dump_https_stat_to_screen (FILE* fout,
                                 char* protocol,
                                 stat_point_https* sd,
                                 unsigned long period)
{
  fprintf(fout, "%sReq:%ld,1xx:%ld,2xx:%ld,3xx:%ld,4xx:%ld,5xx:%ld,Err:%ld,T-Err:%ld,"
          "D:%ldms,D-2xx:%ldms,Ti:%lldB/s,To:%lldB/s\n",
          protocol, sd->requests, sd->resp_1xx, sd->resp_2xx, sd->resp_3xx,
          sd->resp_4xx, sd->resp_5xx, sd->other_errs, sd->url_timeout_errs, sd->appl_delay,
          sd->appl_delay_2xx, sd->data_in/period, sd->data_out/period);
}
static void dump_ftp_stat_to_screen (FILE* fout,
                                 char* protocol,
                                 stat_point_ftp* sd,
                                 unsigned long period)
{
  fprintf(fout, "%sReq:%ld,1xx:%ld,2xx:%ld,3xx:%ld,4xx:%ld,5xx:%ld,6xx:%ld,Err:%ld,T-Err:%ld,"
          "D:%ldms,D-2xx:%ldms,Ti:%lldB/s,To:%lldB/s\n",
          protocol, sd->requests, sd->resp_1xx, sd->resp_2xx, sd->resp_3xx,
          sd->resp_4xx, sd->resp_5xx, sd->resp_6xx, sd->other_errs, sd->url_timeout_errs, sd->appl_delay,
          sd->appl_delay_2xx, sd->data_in/period, sd->data_out/period);
}
static void dump_ftps_stat_to_screen (FILE* fout,
                                 char* protocol,
                                 stat_point_ftps* sd,
                                 unsigned long period)
{
  fprintf(fout, "%sReq:%ld,1xx:%ld,2xx:%ld,3xx:%ld,4xx:%ld,5xx:%ld,6xx:%ld,Err:%ld,T-Err:%ld,"
          "D:%ldms,D-2xx:%ldms,Ti:%lldB/s,To:%lldB/s\n",
          protocol, sd->requests, sd->resp_1xx, sd->resp_2xx, sd->resp_3xx,
          sd->resp_4xx, sd->resp_5xx, sd->resp_6xx, sd->other_errs, sd->url_timeout_errs, sd->appl_delay,
          sd->appl_delay_2xx, sd->data_in/period, sd->data_out/period);
}
static void dump_imap_stat_to_screen (FILE* fout,
                                 char* protocol,
                                 stat_point_imap* sd,
                                 unsigned long period)
{
  fprintf(fout, "%sReq:%ld,Err:%ld,T-Err:%ld,"
          "D:%ldms,Ti:%lldB/s,To:%lldB/s\n",
          protocol, sd->requests, sd->other_errs, sd->url_timeout_errs, sd->appl_delay,
          sd->data_in/period, sd->data_out/period);
}
static void dump_pop3_stat_to_screen (FILE* fout,
                                 char* protocol,
                                 stat_point_pop3* sd,
                                 unsigned long period)
{
  fprintf(fout, "%sReq:%ld,Err:%ld,T-Err:%ld,"
          "D:%ldms,Ti:%lldB/s,To:%lldB/s\n",
          protocol, sd->requests, sd->other_errs, sd->url_timeout_errs, sd->appl_delay,
          sd->data_in/period, sd->data_out/period);
}
static void dump_smtp_stat_to_screen (FILE* fout,
                                 char* protocol,
                                 stat_point_smtp* sd,
                                 unsigned long period)
{
  fprintf(fout, "%sReq:%ld,2xx:%ld,3xx:%ld,4xx:%ld,5xx:%ld,Err:%ld,T-Err:%ld,"
          "D:%ldms,D-2xx:%ldms,Ti:%lldB/s,To:%lldB/s\n",
          protocol, sd->requests, sd->resp_2xx, sd->resp_3xx,
          sd->resp_4xx, sd->resp_5xx, sd->other_errs, sd->url_timeout_errs, sd->appl_delay,
          sd->appl_delay_2xx, sd->data_in/period, sd->data_out/period);
}
/****************************************************************************************
* Function name - print_statistics_header
*
* Description - Prints to a file header for statistics numbers, describing counters
* Input -       *file - open file pointer
* Return Code/Output - None
****************************************************************************************/
void print_statistics_header (FILE* file)
{
    fprintf (file, 
             "RunTime(sec),Appl,Clients,Req,1xx,2xx,3xx,4xx,5xx,Err,T-Err,D,D-2xx,Ti,To\n");
    fflush (file);
}

/***********************************************************************************
* Function name - print_operational_statistics
*
* Description - writes number of login, UAS - for each URL and logoff operations
*               success and failure numbers
*
* Input -       *opstats_file - FILE pointer
*               *osp_curr - pointer to the current operational statistics point
*               *osp_total - pointer to the current operational statistics point
*
* Return Code/Output - None
*************************************************************************************/
static void print_operational_statistics (FILE *opstats_file,
                                          op_stat_point*const osp_curr,
                                          op_stat_point*const osp_total,
                                          url_context* url_arr)
{
  if (!osp_curr || !osp_total || !opstats_file)
    return;

  (void)fprintf (opstats_file,
    " Operations:\t\t Success\t\t Failed\t\t\tTimed out\n");

  if (osp_curr->url_num && (osp_curr->url_num == osp_total->url_num))
    {
      unsigned long i;
      for (i = 0; i < osp_curr->url_num; i++)
        {
          (void)fprintf (opstats_file,
              "URL%ld:%-12.12s\t%-6ld %-8ld\t\t%-6ld %-8ld\t\t%-6ld %-8ld\n",
                   i, url_arr[i].url_short_name, 
                   osp_curr->url_ok[i], osp_total->url_ok[i],
                   osp_curr->url_failed[i], osp_total->url_failed[i],
                   osp_curr->url_timeouted[i], osp_total->url_timeouted[i]);
        }
    }
}

/****************************************************************************************
* Function name - get_average_time_after_url_sleep
*
* Description - Gets average url sleep timeout from batch context
*
* Input -       *bctx - pointer to batch structure
* Return Code/Output - timeout in ms
****************************************************************************************/
unsigned long get_average_time_after_url_sleep(struct batch_context *bctx) 
{
  unsigned long timeout = 0;
  int i;
  for (i = 0; i < bctx->urls_num; i++) 
  {
    timeout += bctx->url_ctx_array[i].timer_after_url_sleep_lrange;
  }
  return timeout / bctx->urls_num;
}