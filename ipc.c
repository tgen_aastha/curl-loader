// must be first include
#include "fdsetsize.h"

#include <string.h>
#include <nanomsg/nn.h>
#include <stdlib.h>

#include "ipc.h"
#include "conf.h"
#include "batch.h" 
#include "jsongen.h"
#include "json.h"

static double convertToSec(const unsigned long ms) {
  double sec = (double)(ms) / 1000;
  return !sec ? 1 : sec;
}

static int build_json_stat(char *buffer, int index, batch_context* bctx, unsigned long now, int current_clients, int max_clients)
{
  const unsigned long delta_time = now - bctx->last_measure;
  double seconds_delta = convertToSec(delta_time);
  const unsigned long caps_current = delta_time == 0 ? 0 : bctx->op_delta.call_init_count*1000/delta_time;

  const unsigned long runtime = now - bctx->start_time;
  double seconds_run = convertToSec(runtime);
  const unsigned long caps_average = runtime == 0 ? 0 : bctx->op_total.call_init_count*1000/runtime;
  unsigned long ii = 0;

  JSON_WITH(buffer);
  JSON_START(buffer);
  JSON_PUT_OBJECT_START(buffer, header);
    JSON_PUT_INT(buffer, type, MSG_TYPE_EVENT_STATS);
  JSON_OBJECT_END(buffer);
  JSON_PUT_OBJECT_START(buffer, body);
    JSON_PUT_INT(buffer, index, index);
    JSON_PUT_OBJECT_START(buffer, interval);
      JSON_PUT_LONG_INT(buffer, delta_time, delta_time);
      JSON_PUT_LONG_INT(buffer, caps, caps_current);
      JSON_PUT_ARRAY_START(buffer, url_ok);
        for (ii=0; ii < bctx->op_delta.url_num; ii++) {
          JSON_LONG_INT(buffer, bctx->op_delta.url_ok[ii]);
        }
      JSON_ARRAY_END(buffer);
      JSON_PUT_ARRAY_START(buffer, url_failed);
        for (ii=0; ii < bctx->op_delta.url_num; ii++) {
          JSON_LONG_INT(buffer, bctx->op_delta.url_failed[ii]);
        }
      JSON_ARRAY_END(buffer);
      JSON_PUT_ARRAY_START(buffer, url_timeouted);
        for (ii=0; ii < bctx->op_delta.url_num; ii++) {
          JSON_LONG_INT(buffer, bctx->op_delta.url_timeouted[ii]);
        }
      JSON_ARRAY_END(buffer);
      JSON_PUT_OBJECT_START(buffer, http);
        JSON_PUT_LONG_INT(buffer, requests, bctx->http_delta.requests);
        JSON_PUT_LONG_INT(buffer, connections, bctx->http_delta.connections);
        JSON_PUT_LONG_INT(buffer, resp_1xx, bctx->http_delta.resp_1xx);
        JSON_PUT_LONG_INT(buffer, resp_2xx, bctx->http_delta.resp_2xx);
        JSON_PUT_LONG_INT(buffer, resp_3xx, bctx->http_delta.resp_3xx);
        JSON_PUT_LONG_INT(buffer, resp_4xx, bctx->http_delta.resp_4xx);
        JSON_PUT_LONG_INT(buffer, resp_5xx, bctx->http_delta.resp_5xx);
        JSON_PUT_LONG_INT(buffer, timeouts, bctx->http_delta.url_timeout_errs);
        JSON_PUT_LONG_INT(buffer, other_errors, bctx->http_delta.other_errs);
      if (bctx->http_delta.other_errs != 0) {
        JSON_PUT_ARRAY_START(buffer, errors);
          for (ii=0; ii < CURL_LAST; ii++) {
            if (ii != CURLE_OK && bctx->http_delta.errs_by_type[ii] != 0) {
              JSON_OBJECT_START(buffer);
                JSON_PUT_LONG_INT(buffer, index, ii);
                JSON_PUT_STRING(buffer, description, curl_easy_strerror(ii));
                JSON_PUT_LONG_INT(buffer, count, bctx->http_delta.errs_by_type[ii]);
              JSON_OBJECT_END(buffer);
            }
          }
        JSON_ARRAY_END(buffer);
      }
        JSON_PUT_LONG_INT(buffer, delay, bctx->http_delta.appl_delay);
        JSON_PUT_LONG_INT(buffer, max_delay, bctx->http_delta.max_appl_delay);
        JSON_PUT_LONG_INT(buffer, min_delay, bctx->http_delta.min_appl_delay);
        JSON_PUT_INT(buffer, delay_points, bctx->http_delta.appl_delay_points);
        JSON_PUT_LONG_INT(buffer, delay_2xx, bctx->http_delta.appl_delay_2xx);
        JSON_PUT_INT(buffer, delay_2xx_points, bctx->http_delta.appl_delay_2xx_points);
        JSON_PUT_LONG_LONG_INT(buffer, data_out, bctx->http_delta.data_out);
        JSON_PUT_LONG_LONG_INT(buffer, data_in, bctx->http_delta.data_in);
        JSON_PUT_DOUBLE(buffer, data_out_speed, (double)bctx->http_delta.data_out / seconds_delta);
        JSON_PUT_DOUBLE(buffer, data_in_speed, (double)bctx->http_delta.data_in / seconds_delta);
      JSON_OBJECT_END(buffer);
      JSON_PUT_OBJECT_START(buffer, https);
        JSON_PUT_LONG_INT(buffer, requests, bctx->https_delta.requests);
        JSON_PUT_LONG_INT(buffer, connections, bctx->https_delta.connections);
        JSON_PUT_LONG_INT(buffer, resp_1xx, bctx->https_delta.resp_1xx);
        JSON_PUT_LONG_INT(buffer, resp_2xx, bctx->https_delta.resp_2xx);
        JSON_PUT_LONG_INT(buffer, resp_3xx, bctx->https_delta.resp_3xx);
        JSON_PUT_LONG_INT(buffer, resp_4xx, bctx->https_delta.resp_4xx);
        JSON_PUT_LONG_INT(buffer, resp_5xx, bctx->https_delta.resp_5xx);
        JSON_PUT_LONG_INT(buffer, timeouts, bctx->https_delta.url_timeout_errs);
        JSON_PUT_LONG_INT(buffer, other_errors, bctx->https_delta.other_errs);
      if (bctx->https_delta.other_errs != 0) {
        JSON_PUT_ARRAY_START(buffer, errors);
          for (ii=0; ii < CURL_LAST; ii++) {
            if (ii != CURLE_OK && bctx->https_delta.errs_by_type[ii] != 0) {
              JSON_OBJECT_START(buffer);
                JSON_PUT_LONG_INT(buffer, index, ii);
                JSON_PUT_STRING(buffer, description, curl_easy_strerror(ii));
                JSON_PUT_LONG_INT(buffer, count, bctx->https_delta.errs_by_type[ii]);
              JSON_OBJECT_END(buffer);
            }
          }
        JSON_ARRAY_END(buffer);
      }
        JSON_PUT_LONG_INT(buffer, delay, bctx->https_delta.appl_delay);
        JSON_PUT_LONG_INT(buffer, max_delay, bctx->https_delta.max_appl_delay);
        JSON_PUT_LONG_INT(buffer, min_delay, bctx->https_delta.min_appl_delay);
        JSON_PUT_INT(buffer, delay_points, bctx->https_delta.appl_delay_points);
        JSON_PUT_LONG_INT(buffer, delay_2xx, bctx->https_delta.appl_delay_2xx);
        JSON_PUT_INT(buffer, delay_2xx_points, bctx->https_delta.appl_delay_2xx_points);
        JSON_PUT_LONG_LONG_INT(buffer, data_out, bctx->https_delta.data_out);
        JSON_PUT_LONG_LONG_INT(buffer, data_in, bctx->https_delta.data_in);
        JSON_PUT_DOUBLE(buffer, data_out_speed, (double)bctx->https_delta.data_out / seconds_delta);
        JSON_PUT_DOUBLE(buffer, data_in_speed, (double)bctx->https_delta.data_in / seconds_delta);
      JSON_OBJECT_END(buffer);
      JSON_PUT_OBJECT_START(buffer, ftp);
        JSON_PUT_LONG_INT(buffer, requests, bctx->ftp_delta.requests);
        JSON_PUT_LONG_INT(buffer, connections, bctx->ftp_delta.connections);
        JSON_PUT_LONG_INT(buffer, resp_1xx, bctx->ftp_delta.resp_1xx);
        JSON_PUT_LONG_INT(buffer, resp_2xx, bctx->ftp_delta.resp_2xx);
        JSON_PUT_LONG_INT(buffer, resp_3xx, bctx->ftp_delta.resp_3xx);
        JSON_PUT_LONG_INT(buffer, resp_4xx, bctx->ftp_delta.resp_4xx);
        JSON_PUT_LONG_INT(buffer, resp_5xx, bctx->ftp_delta.resp_5xx);
        JSON_PUT_LONG_INT(buffer, resp_6xx, bctx->ftp_delta.resp_6xx);
        JSON_PUT_LONG_INT(buffer, timeouts, bctx->ftp_delta.url_timeout_errs);
        JSON_PUT_LONG_INT(buffer, other_errors, bctx->ftp_delta.other_errs);
      if (bctx->ftp_delta.other_errs != 0) {
        JSON_PUT_ARRAY_START(buffer, errors);
          for (ii=0; ii < CURL_LAST; ii++) {
            if (ii != CURLE_OK && bctx->ftp_delta.errs_by_type[ii] != 0) {
              JSON_OBJECT_START(buffer);
                JSON_PUT_LONG_INT(buffer, index, ii);
                JSON_PUT_STRING(buffer, description, curl_easy_strerror(ii));
                JSON_PUT_LONG_INT(buffer, count, bctx->ftp_delta.errs_by_type[ii]);
              JSON_OBJECT_END(buffer);
            }
          }
        JSON_ARRAY_END(buffer);
      }
        JSON_PUT_LONG_INT(buffer, delay, bctx->ftp_delta.appl_delay);
        JSON_PUT_LONG_INT(buffer, max_delay, bctx->ftp_delta.max_appl_delay);
        JSON_PUT_LONG_INT(buffer, min_delay, bctx->ftp_delta.min_appl_delay);
        JSON_PUT_INT(buffer, delay_points, bctx->ftp_delta.appl_delay_points);
        JSON_PUT_LONG_INT(buffer, delay_2xx, bctx->ftp_delta.appl_delay_2xx);
        JSON_PUT_INT(buffer, delay_2xx_points, bctx->ftp_delta.appl_delay_2xx_points);
        JSON_PUT_LONG_LONG_INT(buffer, data_out, bctx->ftp_delta.data_out);
        JSON_PUT_LONG_LONG_INT(buffer, data_in, bctx->ftp_delta.data_in);
        JSON_PUT_DOUBLE(buffer, data_out_speed, (double)bctx->ftp_delta.data_out / seconds_delta);
        JSON_PUT_DOUBLE(buffer, data_in_speed, (double)bctx->ftp_delta.data_in / seconds_delta);
      JSON_OBJECT_END(buffer);
      JSON_PUT_OBJECT_START(buffer, ftps);
        JSON_PUT_LONG_INT(buffer, requests, bctx->ftps_delta.requests);
        JSON_PUT_LONG_INT(buffer, connections, bctx->ftps_delta.connections);
        JSON_PUT_LONG_INT(buffer, resp_1xx, bctx->ftps_delta.resp_1xx);
        JSON_PUT_LONG_INT(buffer, resp_2xx, bctx->ftps_delta.resp_2xx);
        JSON_PUT_LONG_INT(buffer, resp_3xx, bctx->ftps_delta.resp_3xx);
        JSON_PUT_LONG_INT(buffer, resp_4xx, bctx->ftps_delta.resp_4xx);
        JSON_PUT_LONG_INT(buffer, resp_5xx, bctx->ftps_delta.resp_5xx);
        JSON_PUT_LONG_INT(buffer, resp_6xx, bctx->ftps_delta.resp_6xx);
        JSON_PUT_LONG_INT(buffer, timeouts, bctx->ftps_delta.url_timeout_errs);
        JSON_PUT_LONG_INT(buffer, other_errors, bctx->ftps_delta.other_errs);
      if (bctx->ftps_delta.other_errs != 0) {
        JSON_PUT_ARRAY_START(buffer, errors);
          for (ii=0; ii < CURL_LAST; ii++) {
            if (ii != CURLE_OK && bctx->ftps_delta.errs_by_type[ii] != 0) {
              JSON_OBJECT_START(buffer);
                JSON_PUT_LONG_INT(buffer, index, ii);
                JSON_PUT_STRING(buffer, description, curl_easy_strerror(ii));
                JSON_PUT_LONG_INT(buffer, count, bctx->ftps_delta.errs_by_type[ii]);
              JSON_OBJECT_END(buffer);
            }
          }
        JSON_ARRAY_END(buffer);
      }
        JSON_PUT_LONG_INT(buffer, delay, bctx->ftps_delta.appl_delay);
        JSON_PUT_LONG_INT(buffer, max_delay, bctx->ftps_delta.max_appl_delay);
        JSON_PUT_LONG_INT(buffer, min_delay, bctx->ftps_delta.min_appl_delay);
        JSON_PUT_INT(buffer, delay_points, bctx->ftps_delta.appl_delay_points);
        JSON_PUT_LONG_INT(buffer, delay_2xx, bctx->ftps_delta.appl_delay_2xx);
        JSON_PUT_INT(buffer, delay_2xx_points, bctx->ftps_delta.appl_delay_2xx_points);
        JSON_PUT_LONG_LONG_INT(buffer, data_out, bctx->ftps_delta.data_out);
        JSON_PUT_LONG_LONG_INT(buffer, data_in, bctx->ftps_delta.data_in);
        JSON_PUT_DOUBLE(buffer, data_out_speed, (double)bctx->ftps_delta.data_out / seconds_delta);
        JSON_PUT_DOUBLE(buffer, data_in_speed, (double)bctx->ftps_delta.data_in / seconds_delta);
      JSON_OBJECT_END(buffer);
      JSON_PUT_OBJECT_START(buffer, imap);
        JSON_PUT_LONG_INT(buffer, requests, bctx->imap_delta.requests);
        JSON_PUT_LONG_INT(buffer, connections, bctx->imap_delta.connections);
        JSON_PUT_LONG_INT(buffer, timeouts, bctx->imap_delta.url_timeout_errs);
        JSON_PUT_LONG_INT(buffer, other_errors, bctx->imap_delta.other_errs);
      if (bctx->imap_delta.other_errs != 0) {
        JSON_PUT_ARRAY_START(buffer, errors);
          for (ii=0; ii < CURL_LAST; ii++) {
            if (ii != CURLE_OK && bctx->imap_delta.errs_by_type[ii] != 0) {
              JSON_OBJECT_START(buffer);
                JSON_PUT_LONG_INT(buffer, index, ii);
                JSON_PUT_STRING(buffer, description, curl_easy_strerror(ii));
                JSON_PUT_LONG_INT(buffer, count, bctx->imap_delta.errs_by_type[ii]);
              JSON_OBJECT_END(buffer);
            }
          }
        JSON_ARRAY_END(buffer);
      }
        JSON_PUT_LONG_INT(buffer, delay, bctx->imap_delta.appl_delay);
        JSON_PUT_LONG_INT(buffer, max_delay, bctx->imap_delta.max_appl_delay);
        JSON_PUT_LONG_INT(buffer, min_delay, bctx->imap_delta.min_appl_delay);
        JSON_PUT_INT(buffer, delay_points, bctx->imap_delta.appl_delay_points);
        JSON_PUT_LONG_LONG_INT(buffer, data_out, bctx->imap_delta.data_out);
        JSON_PUT_LONG_LONG_INT(buffer, data_in, bctx->imap_delta.data_in);
        JSON_PUT_DOUBLE(buffer, data_out_speed, (double)bctx->imap_delta.data_out / seconds_delta);
        JSON_PUT_DOUBLE(buffer, data_in_speed, (double)bctx->imap_delta.data_in / seconds_delta);
      JSON_OBJECT_END(buffer);
      JSON_PUT_OBJECT_START(buffer, pop3);
        JSON_PUT_LONG_INT(buffer, requests, bctx->pop3_delta.requests);
        JSON_PUT_LONG_INT(buffer, connections, bctx->pop3_delta.connections);
        JSON_PUT_LONG_INT(buffer, timeouts, bctx->pop3_delta.url_timeout_errs);
        JSON_PUT_LONG_INT(buffer, other_errors, bctx->pop3_delta.other_errs);
      if (bctx->pop3_delta.other_errs != 0) {
        JSON_PUT_ARRAY_START(buffer, errors);
          for (ii=0; ii < CURL_LAST; ii++) {
            if (ii != CURLE_OK && bctx->pop3_delta.errs_by_type[ii] != 0) {
              JSON_OBJECT_START(buffer);
                JSON_PUT_LONG_INT(buffer, index, ii);
                JSON_PUT_STRING(buffer, description, curl_easy_strerror(ii));
                JSON_PUT_LONG_INT(buffer, count, bctx->pop3_delta.errs_by_type[ii]);
              JSON_OBJECT_END(buffer);
            }
          }
        JSON_ARRAY_END(buffer);
      }
        JSON_PUT_LONG_INT(buffer, delay, bctx->pop3_delta.appl_delay);
        JSON_PUT_LONG_INT(buffer, max_delay, bctx->pop3_delta.max_appl_delay);
        JSON_PUT_LONG_INT(buffer, min_delay, bctx->pop3_delta.min_appl_delay);
        JSON_PUT_INT(buffer, delay_points, bctx->pop3_delta.appl_delay_points);
        JSON_PUT_LONG_LONG_INT(buffer, data_out, bctx->pop3_delta.data_out);
        JSON_PUT_LONG_LONG_INT(buffer, data_in, bctx->pop3_delta.data_in);
        JSON_PUT_DOUBLE(buffer, data_out_speed, (double)bctx->pop3_delta.data_out / seconds_delta);
        JSON_PUT_DOUBLE(buffer, data_in_speed, (double)bctx->pop3_delta.data_in / seconds_delta);
      JSON_OBJECT_END(buffer);
      JSON_PUT_OBJECT_START(buffer, smtp);
        JSON_PUT_LONG_INT(buffer, requests, bctx->smtp_delta.requests);
        JSON_PUT_LONG_INT(buffer, connections, bctx->smtp_delta.connections);
        JSON_PUT_LONG_INT(buffer, resp_2xx, bctx->smtp_delta.resp_2xx);
        JSON_PUT_LONG_INT(buffer, resp_3xx, bctx->smtp_delta.resp_3xx);
        JSON_PUT_LONG_INT(buffer, resp_4xx, bctx->smtp_delta.resp_4xx);
        JSON_PUT_LONG_INT(buffer, resp_5xx, bctx->smtp_delta.resp_5xx);
        JSON_PUT_LONG_INT(buffer, timeouts, bctx->smtp_delta.url_timeout_errs);
        JSON_PUT_LONG_INT(buffer, other_errors, bctx->smtp_delta.other_errs);
      if (bctx->smtp_delta.other_errs != 0) {
        JSON_PUT_ARRAY_START(buffer, errors);
          for (ii=0; ii < CURL_LAST; ii++) {
            if (ii != CURLE_OK && bctx->smtp_delta.errs_by_type[ii] != 0) {
              JSON_OBJECT_START(buffer);
                JSON_PUT_LONG_INT(buffer, index, ii);
                JSON_PUT_STRING(buffer, description, curl_easy_strerror(ii));
                JSON_PUT_LONG_INT(buffer, count, bctx->smtp_delta.errs_by_type[ii]);
              JSON_OBJECT_END(buffer);
            }
          }
        JSON_ARRAY_END(buffer);
      }
        JSON_PUT_LONG_INT(buffer, delay, bctx->smtp_delta.appl_delay);
        JSON_PUT_LONG_INT(buffer, max_delay, bctx->smtp_delta.max_appl_delay);
        JSON_PUT_LONG_INT(buffer, min_delay, bctx->smtp_delta.min_appl_delay);
        JSON_PUT_INT(buffer, delay_points, bctx->smtp_delta.appl_delay_points);
        JSON_PUT_LONG_INT(buffer, delay_2xx, bctx->smtp_delta.appl_delay_2xx);
        JSON_PUT_INT(buffer, delay_2xx_points, bctx->smtp_delta.appl_delay_2xx_points);
        JSON_PUT_LONG_LONG_INT(buffer, data_out, bctx->smtp_delta.data_out);
        JSON_PUT_LONG_LONG_INT(buffer, data_in, bctx->smtp_delta.data_in);
        JSON_PUT_DOUBLE(buffer, data_out_speed, (double)bctx->smtp_delta.data_out / seconds_delta);
        JSON_PUT_DOUBLE(buffer, data_in_speed, (double)bctx->smtp_delta.data_in / seconds_delta);
      JSON_OBJECT_END(buffer);
    JSON_OBJECT_END(buffer);
    JSON_PUT_OBJECT_START(buffer, total);
      JSON_PUT_LONG_INT(buffer, runtime, runtime);
      JSON_PUT_INT(buffer, current_clients, current_clients);
      JSON_PUT_INT(buffer, max_clients, max_clients);
      JSON_PUT_LONG_INT(buffer, caps, caps_average);
      JSON_PUT_LONG_INT(buffer, manual_control, bctx->stop_client_num_gradual_increase);
      JSON_PUT_ARRAY_START(buffer, url_ok);
        for (ii=0; ii < bctx->op_total.url_num; ii++) {
          JSON_LONG_INT(buffer, bctx->op_total.url_ok[ii]);
        }
      JSON_ARRAY_END(buffer);
      JSON_PUT_ARRAY_START(buffer, url_failed);
        for (ii=0; ii < bctx->op_total.url_num; ii++) {
          JSON_LONG_INT(buffer, bctx->op_total.url_failed[ii]);
        }
      JSON_ARRAY_END(buffer);
      JSON_PUT_ARRAY_START(buffer, url_timeouted);
        for (ii=0; ii < bctx->op_total.url_num; ii++) {
          JSON_LONG_INT(buffer, bctx->op_total.url_timeouted[ii]);
        }
      JSON_ARRAY_END(buffer);
      JSON_PUT_OBJECT_START(buffer, http);
        JSON_PUT_LONG_INT(buffer, requests, bctx->http_total.requests);
        JSON_PUT_LONG_INT(buffer, connections, bctx->http_total.connections);
        JSON_PUT_LONG_INT(buffer, resp_1xx, bctx->http_total.resp_1xx);
        JSON_PUT_LONG_INT(buffer, resp_2xx, bctx->http_total.resp_2xx);
        JSON_PUT_LONG_INT(buffer, resp_3xx, bctx->http_total.resp_3xx);
        JSON_PUT_LONG_INT(buffer, resp_4xx, bctx->http_total.resp_4xx);
        JSON_PUT_LONG_INT(buffer, resp_5xx, bctx->http_total.resp_5xx);
        JSON_PUT_LONG_INT(buffer, timeouts, bctx->http_total.url_timeout_errs);
        JSON_PUT_LONG_INT(buffer, other_errors, bctx->http_total.other_errs);
      if (bctx->http_total.other_errs != 0) {
        JSON_PUT_ARRAY_START(buffer, errors);
          for (ii=0; ii < CURL_LAST; ii++) {
            if (ii != CURLE_OK && bctx->http_total.errs_by_type[ii] != 0) {
              JSON_OBJECT_START(buffer);
                JSON_PUT_LONG_INT(buffer, index, ii);
                JSON_PUT_STRING(buffer, description, curl_easy_strerror(ii));
                JSON_PUT_LONG_INT(buffer, count, bctx->http_total.errs_by_type[ii]);
              JSON_OBJECT_END(buffer);
            }
          }
        JSON_ARRAY_END(buffer);
      }
        JSON_PUT_LONG_INT(buffer, delay, bctx->http_total.appl_delay);
        JSON_PUT_LONG_INT(buffer, max_delay, bctx->http_total.max_appl_delay);
        JSON_PUT_LONG_INT(buffer, min_delay, bctx->http_total.min_appl_delay);
        JSON_PUT_INT(buffer, delay_points, bctx->http_total.appl_delay_points);
        JSON_PUT_LONG_INT(buffer, delay_2xx, bctx->http_total.appl_delay_2xx);
        JSON_PUT_INT(buffer, delay_2xx_points, bctx->http_total.appl_delay_2xx_points);
        JSON_PUT_LONG_LONG_INT(buffer, data_out, bctx->http_total.data_out);
        JSON_PUT_LONG_LONG_INT(buffer, data_in, bctx->http_total.data_in);
        JSON_PUT_DOUBLE(buffer, data_out_speed, (double)bctx->http_total.data_out / seconds_run);
        JSON_PUT_DOUBLE(buffer, data_in_speed, (double)bctx->http_total.data_in / seconds_run);
      JSON_OBJECT_END(buffer);
      JSON_PUT_OBJECT_START(buffer, https);
        JSON_PUT_LONG_INT(buffer, requests, bctx->https_total.requests);
        JSON_PUT_LONG_INT(buffer, connections, bctx->https_total.connections);
        JSON_PUT_LONG_INT(buffer, resp_1xx, bctx->https_total.resp_1xx);
        JSON_PUT_LONG_INT(buffer, resp_2xx, bctx->https_total.resp_2xx);
        JSON_PUT_LONG_INT(buffer, resp_3xx, bctx->https_total.resp_3xx);
        JSON_PUT_LONG_INT(buffer, resp_4xx, bctx->https_total.resp_4xx);
        JSON_PUT_LONG_INT(buffer, resp_5xx, bctx->https_total.resp_5xx);
        JSON_PUT_LONG_INT(buffer, timeouts, bctx->https_total.url_timeout_errs);
        JSON_PUT_LONG_INT(buffer, other_errors, bctx->https_total.other_errs);
      if (bctx->https_total.other_errs != 0) {
        JSON_PUT_ARRAY_START(buffer, errors);
          for (ii=0; ii < CURL_LAST; ii++) {
            if (ii != CURLE_OK && bctx->https_total.errs_by_type[ii] != 0) {
              JSON_OBJECT_START(buffer);
                JSON_PUT_LONG_INT(buffer, index, ii);
                JSON_PUT_STRING(buffer, description, curl_easy_strerror(ii));
                JSON_PUT_LONG_INT(buffer, count, bctx->https_total.errs_by_type[ii]);
              JSON_OBJECT_END(buffer);
            }
          }
        JSON_ARRAY_END(buffer);
      }
        JSON_PUT_LONG_INT(buffer, delay, bctx->https_total.appl_delay);
        JSON_PUT_LONG_INT(buffer, max_delay, bctx->https_total.max_appl_delay);
        JSON_PUT_LONG_INT(buffer, min_delay, bctx->https_total.min_appl_delay);
        JSON_PUT_INT(buffer, delay_points, bctx->https_total.appl_delay_points);
        JSON_PUT_LONG_INT(buffer, delay_2xx, bctx->https_total.appl_delay_2xx);
        JSON_PUT_INT(buffer, delay_2xx_points, bctx->https_total.appl_delay_2xx_points);
        JSON_PUT_LONG_LONG_INT(buffer, data_out, bctx->https_total.data_out);
        JSON_PUT_LONG_LONG_INT(buffer, data_in, bctx->https_total.data_in);
        JSON_PUT_DOUBLE(buffer, data_out_speed, (double)bctx->https_total.data_out / seconds_run);
        JSON_PUT_DOUBLE(buffer, data_in_speed, (double)bctx->https_total.data_in / seconds_run);
      JSON_OBJECT_END(buffer);
      JSON_PUT_OBJECT_START(buffer, ftp);
        JSON_PUT_LONG_INT(buffer, requests, bctx->ftp_total.requests);
        JSON_PUT_LONG_INT(buffer, connections, bctx->ftp_total.connections);
        JSON_PUT_LONG_INT(buffer, resp_1xx, bctx->ftp_total.resp_1xx);
        JSON_PUT_LONG_INT(buffer, resp_2xx, bctx->ftp_total.resp_2xx);
        JSON_PUT_LONG_INT(buffer, resp_3xx, bctx->ftp_total.resp_3xx);
        JSON_PUT_LONG_INT(buffer, resp_4xx, bctx->ftp_total.resp_4xx);
        JSON_PUT_LONG_INT(buffer, resp_5xx, bctx->ftp_total.resp_5xx);
        JSON_PUT_LONG_INT(buffer, resp_6xx, bctx->ftp_total.resp_6xx);
        JSON_PUT_LONG_INT(buffer, timeouts, bctx->ftp_total.url_timeout_errs);
        JSON_PUT_LONG_INT(buffer, other_errors, bctx->ftp_total.other_errs);
      if (bctx->ftp_total.other_errs != 0) {
        JSON_PUT_ARRAY_START(buffer, errors);
          for (ii=0; ii < CURL_LAST; ii++) {
            if (ii != CURLE_OK && bctx->ftp_total.errs_by_type[ii] != 0) {
              JSON_OBJECT_START(buffer);
                JSON_PUT_LONG_INT(buffer, index, ii);
                JSON_PUT_STRING(buffer, description, curl_easy_strerror(ii));
                JSON_PUT_LONG_INT(buffer, count, bctx->ftp_total.errs_by_type[ii]);
              JSON_OBJECT_END(buffer);
            }
          }
        JSON_ARRAY_END(buffer);
      }
        JSON_PUT_LONG_INT(buffer, delay, bctx->ftp_total.appl_delay);
        JSON_PUT_LONG_INT(buffer, max_delay, bctx->ftp_total.max_appl_delay);
        JSON_PUT_LONG_INT(buffer, min_delay, bctx->ftp_total.min_appl_delay);
        JSON_PUT_INT(buffer, delay_points, bctx->ftp_total.appl_delay_points);
        JSON_PUT_LONG_INT(buffer, delay_2xx, bctx->ftp_total.appl_delay_2xx);
        JSON_PUT_INT(buffer, delay_2xx_points, bctx->ftp_total.appl_delay_2xx_points);
        JSON_PUT_LONG_LONG_INT(buffer, data_out, bctx->ftp_total.data_out);
        JSON_PUT_LONG_LONG_INT(buffer, data_in, bctx->ftp_total.data_in);
        JSON_PUT_DOUBLE(buffer, data_out_speed, (double)bctx->ftp_total.data_out / seconds_run);
        JSON_PUT_DOUBLE(buffer, data_in_speed, (double)bctx->ftp_total.data_in / seconds_run);
      JSON_OBJECT_END(buffer);
      JSON_PUT_OBJECT_START(buffer, ftps);
        JSON_PUT_LONG_INT(buffer, requests, bctx->ftps_total.requests);
        JSON_PUT_LONG_INT(buffer, connections, bctx->ftps_total.connections);
        JSON_PUT_LONG_INT(buffer, resp_1xx, bctx->ftps_total.resp_1xx);
        JSON_PUT_LONG_INT(buffer, resp_2xx, bctx->ftps_total.resp_2xx);
        JSON_PUT_LONG_INT(buffer, resp_3xx, bctx->ftps_total.resp_3xx);
        JSON_PUT_LONG_INT(buffer, resp_4xx, bctx->ftps_total.resp_4xx);
        JSON_PUT_LONG_INT(buffer, resp_5xx, bctx->ftps_total.resp_5xx);
        JSON_PUT_LONG_INT(buffer, resp_6xx, bctx->ftps_total.resp_6xx);
        JSON_PUT_LONG_INT(buffer, timeouts, bctx->ftps_total.url_timeout_errs);
        JSON_PUT_LONG_INT(buffer, other_errors, bctx->ftps_total.other_errs);
      if (bctx->ftps_total.other_errs != 0) {
        JSON_PUT_ARRAY_START(buffer, errors);
          for (ii=0; ii < CURL_LAST; ii++) {
            if (ii != CURLE_OK && bctx->ftps_total.errs_by_type[ii] != 0) {
              JSON_OBJECT_START(buffer);
                JSON_PUT_LONG_INT(buffer, index, ii);
                JSON_PUT_STRING(buffer, description, curl_easy_strerror(ii));
                JSON_PUT_LONG_INT(buffer, count, bctx->ftps_total.errs_by_type[ii]);
              JSON_OBJECT_END(buffer);
            }
          }
        JSON_ARRAY_END(buffer);
      }
        JSON_PUT_LONG_INT(buffer, delay, bctx->ftps_total.appl_delay);
        JSON_PUT_LONG_INT(buffer, max_delay, bctx->ftps_total.max_appl_delay);
        JSON_PUT_LONG_INT(buffer, min_delay, bctx->ftps_total.min_appl_delay);
        JSON_PUT_INT(buffer, delay_points, bctx->ftps_total.appl_delay_points);
        JSON_PUT_LONG_INT(buffer, delay_2xx, bctx->ftps_total.appl_delay_2xx);
        JSON_PUT_INT(buffer, delay_2xx_points, bctx->ftps_total.appl_delay_2xx_points);
        JSON_PUT_LONG_LONG_INT(buffer, data_out, bctx->ftps_total.data_out);
        JSON_PUT_LONG_LONG_INT(buffer, data_in, bctx->ftps_total.data_in);
        JSON_PUT_DOUBLE(buffer, data_out_speed, (double)bctx->ftps_total.data_out / seconds_run);
        JSON_PUT_DOUBLE(buffer, data_in_speed, (double)bctx->ftps_total.data_in / seconds_run);
      JSON_OBJECT_END(buffer);
      JSON_PUT_OBJECT_START(buffer, imap);
        JSON_PUT_LONG_INT(buffer, requests, bctx->imap_total.requests);
        JSON_PUT_LONG_INT(buffer, connections, bctx->imap_total.connections);
        JSON_PUT_LONG_INT(buffer, timeouts, bctx->imap_total.url_timeout_errs);
        JSON_PUT_LONG_INT(buffer, other_errors, bctx->imap_total.other_errs);
      if (bctx->imap_total.other_errs != 0) {
        JSON_PUT_ARRAY_START(buffer, errors);
          for (ii=0; ii < CURL_LAST; ii++) {
            if (ii != CURLE_OK && bctx->imap_total.errs_by_type[ii] != 0) {
              JSON_OBJECT_START(buffer);
                JSON_PUT_LONG_INT(buffer, index, ii);
                JSON_PUT_STRING(buffer, description, curl_easy_strerror(ii));
                JSON_PUT_LONG_INT(buffer, count, bctx->imap_total.errs_by_type[ii]);
              JSON_OBJECT_END(buffer);
            }
          }
        JSON_ARRAY_END(buffer);
      }
        JSON_PUT_LONG_INT(buffer, delay, bctx->imap_total.appl_delay);
        JSON_PUT_LONG_INT(buffer, max_delay, bctx->imap_total.max_appl_delay);
        JSON_PUT_LONG_INT(buffer, min_delay, bctx->imap_total.min_appl_delay);
        JSON_PUT_INT(buffer, delay_points, bctx->imap_total.appl_delay_points);
        JSON_PUT_LONG_LONG_INT(buffer, data_out, bctx->imap_total.data_out);
        JSON_PUT_LONG_LONG_INT(buffer, data_in, bctx->imap_total.data_in);
        JSON_PUT_DOUBLE(buffer, data_out_speed, (double)bctx->imap_total.data_out / seconds_run);
        JSON_PUT_DOUBLE(buffer, data_in_speed, (double)bctx->imap_total.data_in / seconds_run);
      JSON_OBJECT_END(buffer);
      JSON_PUT_OBJECT_START(buffer, pop3);
        JSON_PUT_LONG_INT(buffer, requests, bctx->pop3_total.requests);
        JSON_PUT_LONG_INT(buffer, connections, bctx->pop3_total.connections);
        JSON_PUT_LONG_INT(buffer, timeouts, bctx->pop3_total.url_timeout_errs);
        JSON_PUT_LONG_INT(buffer, other_errors, bctx->pop3_total.other_errs);
      if (bctx->pop3_total.other_errs != 0) {
        JSON_PUT_ARRAY_START(buffer, errors);
          for (ii=0; ii < CURL_LAST; ii++) {
            if (ii != CURLE_OK && bctx->pop3_total.errs_by_type[ii] != 0) {
              JSON_OBJECT_START(buffer);
                JSON_PUT_LONG_INT(buffer, index, ii);
                JSON_PUT_STRING(buffer, description, curl_easy_strerror(ii));
                JSON_PUT_LONG_INT(buffer, count, bctx->pop3_total.errs_by_type[ii]);
              JSON_OBJECT_END(buffer);
            }
          }
        JSON_ARRAY_END(buffer);
      }
        JSON_PUT_LONG_INT(buffer, delay, bctx->pop3_total.appl_delay);
        JSON_PUT_LONG_INT(buffer, max_delay, bctx->pop3_total.max_appl_delay);
        JSON_PUT_LONG_INT(buffer, min_delay, bctx->pop3_total.min_appl_delay);
        JSON_PUT_INT(buffer, delay_points, bctx->pop3_total.appl_delay_points);
        JSON_PUT_LONG_LONG_INT(buffer, data_out, bctx->pop3_total.data_out);
        JSON_PUT_LONG_LONG_INT(buffer, data_in, bctx->pop3_total.data_in);
        JSON_PUT_DOUBLE(buffer, data_out_speed, (double)bctx->pop3_total.data_out / seconds_run);
        JSON_PUT_DOUBLE(buffer, data_in_speed, (double)bctx->pop3_total.data_in / seconds_run);
      JSON_OBJECT_END(buffer);
      JSON_PUT_OBJECT_START(buffer, smtp);
        JSON_PUT_LONG_INT(buffer, requests, bctx->smtp_total.requests);
        JSON_PUT_LONG_INT(buffer, connections, bctx->smtp_total.connections);
        JSON_PUT_LONG_INT(buffer, resp_2xx, bctx->smtp_total.resp_2xx);
        JSON_PUT_LONG_INT(buffer, resp_3xx, bctx->smtp_total.resp_3xx);
        JSON_PUT_LONG_INT(buffer, resp_4xx, bctx->smtp_total.resp_4xx);
        JSON_PUT_LONG_INT(buffer, resp_5xx, bctx->smtp_total.resp_5xx);
        JSON_PUT_LONG_INT(buffer, timeouts, bctx->smtp_total.url_timeout_errs);
        JSON_PUT_LONG_INT(buffer, other_errors, bctx->smtp_total.other_errs);
      if (bctx->smtp_total.other_errs != 0) {
        JSON_PUT_ARRAY_START(buffer, errors);
          for (ii=0; ii < CURL_LAST; ii++) {
            if (ii != CURLE_OK && bctx->smtp_total.errs_by_type[ii] != 0) {
              JSON_OBJECT_START(buffer);
                JSON_PUT_LONG_INT(buffer, index, ii);
                JSON_PUT_STRING(buffer, description, curl_easy_strerror(ii));
                JSON_PUT_LONG_INT(buffer, count, bctx->smtp_total.errs_by_type[ii]);
              JSON_OBJECT_END(buffer);
            }
          }
        JSON_ARRAY_END(buffer);
      }
        JSON_PUT_LONG_INT(buffer, delay, bctx->smtp_total.appl_delay);
        JSON_PUT_LONG_INT(buffer, max_delay, bctx->smtp_total.max_appl_delay);
        JSON_PUT_LONG_INT(buffer, min_delay, bctx->smtp_total.min_appl_delay);
        JSON_PUT_INT(buffer, delay_points, bctx->smtp_total.appl_delay_points);
        JSON_PUT_LONG_INT(buffer, delay_2xx, bctx->smtp_total.appl_delay_2xx);
        JSON_PUT_INT(buffer, delay_2xx_points, bctx->smtp_total.appl_delay_2xx_points);
        JSON_PUT_LONG_LONG_INT(buffer, data_out, bctx->smtp_total.data_out);
        JSON_PUT_LONG_LONG_INT(buffer, data_in, bctx->smtp_total.data_in);
        JSON_PUT_DOUBLE(buffer, data_out_speed, (double)bctx->smtp_total.data_out / seconds_run);
        JSON_PUT_DOUBLE(buffer, data_in_speed, (double)bctx->smtp_total.data_in / seconds_run);
      JSON_OBJECT_END(buffer);

    JSON_OBJECT_END(buffer);
  JSON_OBJECT_END(buffer);
  JSON_END(buffer);
  return buffer_len;
}

static int build_json_test_ready(char *buffer)
{
  JSON_WITH(buffer);
  JSON_START(buffer);
  JSON_PUT_OBJECT_START(buffer, header);
    JSON_PUT_INT(buffer, type, MSG_TYPE_EVENT_TEST_READY);
  JSON_OBJECT_END(buffer);
  JSON_END(buffer);
  return buffer_len;
}

static int ipc_receive(char* buf, int buf_len)
{
  if (nn_sock_descr >= 0) {
    const int nbytes = nn_recv(nn_sock_descr, buf, buf_len, NN_DONTWAIT);
    if (nbytes == EAGAIN) {
      return -2;
    }
    return nbytes;
  } else {
    return -1;
  }
}

static void ipc_send(char *buffer, int buffer_len)
{
  if (nn_sock_descr >= 0) {
    if (buffer_len > 0) {
      int bytes = nn_send(nn_sock_descr, buffer, buffer_len, NN_DONTWAIT);
      if (bytes != buffer_len) {
        /* TODO: communication broken - need to handle this error */
      }
    }
  }
}

static void ipc_send_test_ready()
{
    char json[2048];
    int json_len = build_json_test_ready(json);
    ipc_send(json, json_len);
}

static int ipc_receive_start_test()
{
  char json[2048];
  if (nn_sock_descr >= 0) {
    int len = nn_recv(nn_sock_descr, json, 2048, 0);
    if (len > 0) {
      // TODO: parse JSON and check message
      return 0;
    }
    else {
      return -1;
    }
  }
  else {
    return -1;
  }
}

void ipc_send_stat(batch_context* bctx, unsigned long now, int current_clients, int max_clients)
{
    static int index = 0;
    char json[32768];
    int json_len = build_json_stat(json, index++, bctx, now, current_clients, max_clients);
    ipc_send(json, json_len);
}

int ipc_sync()
{
  // Send 'test ready'
  fprintf (stderr, "Sending 'test ready'\n");
  ipc_send_test_ready();
  // Wait for 'start test'
  fprintf (stderr, "Waiting for 'start test'\n");
  if (ipc_receive_start_test() != 0) {
    fprintf (stderr, "Wrong message from peer when waiting for 'start test'\n");
    return -1;
  }
  else {
    fprintf (stderr, "Got 'start test'\n");
    return 0;
  }
}

int ipc_is_enabled()
{
  if (strlen(fifo_filename) == 0) {
    return 0;
  }
  else {
    return 1;
  }
}

static int change_clients_num(batch_context* bctx, const int clients_number) {
  fprintf(stderr, "Manual scale change by %d was scheduled\n", clients_number);
  if (!threads_subbatches_num) {
      bctx->manual_increase_clients_count += clients_number;
      return 0;
  }
  
  const int thread_clients = clients_number / threads_subbatches_num;
  int clients_reminder = clients_number - thread_clients * threads_subbatches_num;
  const int diff = abs(clients_number) / clients_number;

  int i;
  for (i = 0; i < threads_subbatches_num; i++) {
    batch_context* curr_bctx = bctx - bctx->batch_id + i;
    int subbatch_clients = thread_clients;
    if (clients_reminder) {
      subbatch_clients += diff;
      clients_reminder -= diff;
    }
    curr_bctx->manual_increase_clients_count += subbatch_clients;
  }
  return 0;
}

static int change_clients_scale_type(batch_context* bctx, const int type) {
  if (!threads_subbatches_num) {
    bctx->stop_client_num_gradual_increase = type;
    return 0;
  }

  int i;
  for (i = 0; i < threads_subbatches_num; i++) {
    batch_context* curr_bctx = bctx - bctx->batch_id + i;
    curr_bctx->stop_client_num_gradual_increase = type;
  }
  fprintf(stderr, "Manual scale control was %s\n", type ? "enabled" : "disabled");
  return 0;
}

static int build_json_general_response(char *buffer, int res_type, int res_code)
{
  JSON_WITH(buffer);
  JSON_START(buffer);
  JSON_PUT_OBJECT_START(buffer, header);
    JSON_PUT_INT(buffer, type, res_type);
    JSON_OBJECT_END(buffer);
  JSON_PUT_OBJECT_START(buffer, body);
    JSON_PUT_INT(buffer, code, res_code);
  JSON_OBJECT_END(buffer);
  JSON_END(buffer);
  return buffer_len;
}

static void ipc_send_general_response(int res_type, int res_code) {
  char json[2048];
  int json_len = build_json_general_response(json, res_type, res_code);
  ipc_send(json, json_len);
}

static int ipc_clients_num_change(batch_context* bctx, char* buf) {
  int clients = 0;
  const int parsing_number = json_get_int_key(buf, CLIENT_INCREASE_KEY, &clients);

  if (parsing_number != 0 || !clients) {
    return PARSING_FAIL_CODE;
  }

  const int num_exec_code = change_clients_num(bctx, clients);
  if (num_exec_code < 0) {
    fprintf(stderr, "%s - changing scale number failed.\n", __func__);
    ipc_send_general_response(MSG_CHANGE_SCALE_NUMBER, EXEC_FAIL_CODE);
    return EXEC_FAIL_CODE;
  }

  ipc_send_general_response(MSG_CHANGE_SCALE_NUMBER, OK_CODE);
  return OK_CODE;
}

static int ipc_clients_type_change(batch_context* bctx, char* buf) {
  int type = 0;
  const int parsing_type = json_get_int_key(buf, MANUAL_SCALE_CONTROL_KEY, &type);
  if (parsing_type != 0 || !(type == 0 || type == 1)) {
    return PARSING_FAIL_CODE;
  }

  const int type_exec_code = change_clients_scale_type(bctx, type);
  if (type_exec_code < 0) {
    fprintf(stderr, "%s - changing scale type failed.\n", __func__);
    ipc_send_general_response(MSG_CHANGE_SCALE_TYPE, EXEC_FAIL_CODE);
    return EXEC_FAIL_CODE;
  } 
  ipc_send_general_response(MSG_CHANGE_SCALE_TYPE, OK_CODE);
  return OK_CODE;
}

void ipc_clients_change(batch_context* bctx) 
{
  char buf[CLIENTS_JSON_BUFFER_SIZE];
  if (ipc_receive(buf, CLIENTS_JSON_BUFFER_SIZE) >= 0) {
    if (ipc_clients_num_change(bctx, buf) == PARSING_FAIL_CODE) {
      ipc_clients_type_change(bctx, buf);
    }
  }
}